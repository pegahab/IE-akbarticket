#!/usr/bin/env bash

minikube start
kubectl run akbarticket --image=registry.gitlab.com/pegahab/ie-akbarticket/ticketi --port=8080
kubectl expose deployment akbarticket --type=NodePort
minikube ip