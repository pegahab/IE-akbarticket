package DAOMocks;

import Server.Common.DAOs.ReserveDAO;
import Server.Common.Reservation;

import java.sql.SQLException;

/**
 * Created by amiiigh on 5/12/2017.
 */
public class ReserveDAOMock extends ReserveDAO {
    static Reservation reservation;

    @Override
    public void addReservation(Reservation newRes) throws Exception {
        reservation= newRes;
    }

    @Override
    public void updatePassRefCode(Reservation res, String refCode) throws SQLException {
        reservation.setRefCode(refCode);
    }

    @Override
    public Reservation getReservation(String token) throws Exception {
        return reservation;
    }
}
