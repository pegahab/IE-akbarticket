package DAOMocks;

import Server.Common.DAOs.FlightDAO;
import Server.Common.Flight;
import Server.Common.SeatClass;
import utils.Date;
import utils.Pair;
import utils.Time;

import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by amiiigh on 5/12/2017.
 */
public class FlightDAOMock extends FlightDAO {
    Vector<Flight> flights = new Vector<Flight>();

    public FlightDAOMock(){
        flights.add(new Flight("IR", "452", new Date("2017-02-05"), "THR"
                , "MHD", new Time("1740"), new Time("1850"), "M80", "0"));
        flights.get(0).addSeatClass(new Pair<SeatClass, String>(new SeatClass("THR", "MHD", "Y", "IR", "0"), "5"));
        flights.add(new Flight("IR", "822", new Date("2017-02-05"), "THR"
                , "MHD", new Time("0730"), new Time("0840"), "351", "1"));
        flights.get(1).addSeatClass(new Pair<SeatClass, String>(new SeatClass("THR", "MHD", "Y", "IR", "0"), "8"));
    }
    @Override
    public Vector<Flight> searchForFlights(String origCode, String destCode, String date) throws SQLException {
        return flights;
    }

    @Override
    public Flight getFlightByHash(String flightId) throws SQLException {
        return flights.get(Integer.parseInt(flightId));
    }
}
