package DAOMocks;

import Server.Common.DAOs.SeatClassDAO;
import Server.Common.SeatClass;

import java.sql.SQLException;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by amiiigh on 5/12/2017.
 */
public class SeatClassDAOMock extends SeatClassDAO {
    private Vector<SeatClass> seatClasses = new Vector<SeatClass>();
    private int id = 0;
    @Override
    public SeatClass addSeatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        seatClasses.add(new SeatClass(origCode, destCode, airlineCode, className, String.valueOf(id)));
        id ++;
        return seatClasses.get(id-1);
    }

    @Override
    public SeatClass searchForSeatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        SeatClass sc = new SeatClass(origCode, destCode, className, airlineCode, String.valueOf(id));
        id ++;
        sc.updatePrices();
        return sc;
    }

    @Override
    public SeatClass find_seatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        for (SeatClass sc : seatClasses) {
            if (sc.getOrigCode().equals(origCode) && sc.getDestCode().equals(destCode) && sc.getAirlineCode().equals(airlineCode) && sc.getClassName().equals(className))
                return sc;
        }
        return null;
    }

    @Override
    public SeatClass getSeatClassByHash(String seatClassId) throws SQLException {
        for (SeatClass sc : seatClasses) {
            if (sc.getId().equals(seatClassId))
                return sc;
        }
        return null;
    }
}
