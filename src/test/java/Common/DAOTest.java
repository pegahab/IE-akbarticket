package Common;

import Server.Common.DAOs.DAO;
import org.junit.Test;
import utils.Date;

import java.sql.ResultSet;


/**
 * Created by bornarz on 5/10/17.
 */
public class DAOTest {
    @Test
    public void queryTest() throws Exception {
        DAO dao = new DAO();
        ResultSet resultSet = dao.execSelectQuery("F.ID", "FLIGHTS F", "F.AIRLINE_CODE = " + "'IR'"
                + " and F.FLIGHT_NUM = " + "'321'" + " and F.FLIGHT_DATE = '" + new Date("2017-05-03").toDB()
                + "' and F.ORIG_CODE = " + "'THR'" + " and F.DEST_CODE = " + "'MHD'");
        while(resultSet.next()) {
            System.out.println(resultSet.getString("ID"));
        }
    }
}