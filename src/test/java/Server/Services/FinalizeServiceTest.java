package Server.Services;

import DAOMocks.FlightDAOMock;
import DAOMocks.ReserveDAOMock;
import Server.Common.DAOs.FlightDAO;
import Server.Common.DAOs.ReserveDAO;
import Server.Common.DAOs.SeatClassDAO;
import Server.Common.Passenger;
import Server.Common.Ticket;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;
import java.util.Vector;

import static org.junit.Assert.*;

/**
 * Created by amiiigh on 5/12/2017.
 */
public class FinalizeServiceTest {
    @Before
    public void setUp() throws Exception {
        FlightDAO.changeInstance(new FlightDAOMock());
        SeatClassDAO.changeInstance(new SeatClassDAO());
        ReserveDAO.changeInstance(new ReserveDAOMock());
    }

    @Test
    public void finalize() throws Exception {
        Vector<Passenger> passengers = new Vector<Passenger>();
        passengers.add(new Passenger("borna", "arzi", "123", Passenger.PassengerType.ADULT));
        passengers.add(new Passenger("amir", "ghaf", "312", Passenger.PassengerType.INFANT));
        FinalizeService finalizeService = new FinalizeService();
        FinalizeInputs finalizeInputs = new FinalizeInputs();
        finalizeInputs.setAdults("1");
        finalizeInputs.setChilds("0");
        finalizeInputs.setInfants("1");
        finalizeInputs.setClassName("Y");
        finalizeInputs.setFlightHash("0");
        finalizeInputs.setPassengers(passengers);
        Response response = finalizeService.finalize(finalizeInputs);
        System.out.println(response.getEntity());
        Vector<Ticket> tickets = (Vector<Ticket>) response.getEntity();
        assertTrue(tickets.size() == 2);
    }

}