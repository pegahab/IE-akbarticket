package Server.Services;

import DAOMocks.FlightDAOMock;
import DAOMocks.ReserveDAOMock;
import Server.Common.DAOs.FlightDAO;
import Server.Common.DAOs.ReserveDAO;
import Server.Common.DAOs.SeatClassDAO;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

/**
 * Created by amiiigh on 5/12/2017.
 */
public class ReserveServiceTest {
    @Before
    public void setUp() throws Exception {
        FlightDAO.changeInstance(new FlightDAOMock());
        SeatClassDAO.changeInstance(new SeatClassDAO());
        ReserveDAO.changeInstance(new ReserveDAOMock());
    }

    @Test
    public void reserveWithCorrectPrice() throws Exception {
        ReserveService reserveService = new ReserveService();
        ReserveInputs reserveInputs = new ReserveInputs();
        reserveInputs.setAdults("1");
        reserveInputs.setChilds("0");
        reserveInputs.setInfants("1");
        reserveInputs.setClassName("Y");
        reserveInputs.setCost("4000");
        reserveInputs.setFlightHash("0");
        Response response = reserveService.reserve(reserveInputs);
        assertEquals("flight number :0-SeatClass number :0-true",response.getEntity().toString());
    }

    @Test
    public void reserveWithIncorrectPrice() throws Exception {
        ReserveService reserveService = new ReserveService();
        ReserveInputs reserveInputs = new ReserveInputs();
        reserveInputs.setAdults("1");
        reserveInputs.setChilds("0");
        reserveInputs.setInfants("1");
        reserveInputs.setClassName("Y");
        reserveInputs.setCost("5000");
        reserveInputs.setFlightHash("0");
        Response response = reserveService.reserve(reserveInputs);
        assertEquals("flight number :0-SeatClass number :0-false",response.getEntity().toString());
    }

}