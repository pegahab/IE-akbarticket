package Server.Services;

import DAOMocks.FlightDAOMock;
import DAOMocks.ReserveDAOMock;
import Server.Common.DAOs.FlightDAO;
import Server.Common.DAOs.ReserveDAO;
import Server.Common.DAOs.SeatClassDAO;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.*;

/**
 * Created by amiiigh on 5/12/2017.
 */
public class SearchServiceTest {
    @Before
    public void setUp() throws Exception {
        FlightDAO.changeInstance(new FlightDAOMock());
        SeatClassDAO.changeInstance(new SeatClassDAO());
        ReserveDAO.changeInstance(new ReserveDAOMock());
    }

    @Test
    public void search() throws Exception {
        SearchService searchService = new SearchService();
        SearchInputs searchInputs = new SearchInputs();
        searchInputs.setAdults("1");
        searchInputs.setChilds("0");
        searchInputs.setInfants("1");
        searchInputs.setDate("2017-02-05");
        searchInputs.setDst("MHD");
        searchInputs.setSrc("THR");
        Response response = searchService.search(searchInputs);
        assertEquals("[flight number :0, flight number :1]",response.getEntity().toString());
    }

}