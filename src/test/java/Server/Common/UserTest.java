package Server.Common;

import org.junit.Test;

import java.sql.SQLException;
import java.util.Vector;


import static org.junit.Assert.*;

/**
 * Created by sajadmovahedi on 5/21/17.
 */
public class UserTest {
    @Test
    public void main() {
        User u = null;
        try {
            u = User.authenticate("ADMIN", "ADMIN");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        executeCommand(u);
        try {
            u = User.authenticate("SAJAD", "123");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        executeCommand(u);
    }
    private void executeCommand(User u) {
        if(u instanceof CommonUser) {
            System.out.println("common user logged in");
            System.out.println(u.getTicketInfo("8a719838-9cb0-22a1-4706-fe7a653b4843").toString());
        }
        else if(u instanceof Admin) {
            System.out.println("admin logged in");
            Vector<Ticket> T = u.getAllTickets();
            for(Ticket t:T) {
                System.out.println(t.toString());
            }
        }
        else {
            System.out.println("null pointer");
        }
    }
}