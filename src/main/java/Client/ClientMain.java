package Client;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;


public class ClientMain {

    public static void main(String[] args) {
        TransferFrame tf = new TransferFrame(args[0], args[1]);
        tf.setVisible(true);
    }

}
