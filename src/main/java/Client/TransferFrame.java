package Client;


import org.apache.commons.lang3.ArrayUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by bornarz on 2/13/17.
 */
public class TransferFrame extends JFrame{

    private PrintWriter pw;
    private BufferedReader br;
    private JPanel cards;
    private JPanel serviceSelection;
    private JPanel searchPanel;
    private JPanel reservePanel;
    private JPanel finzalizePanel;
    private int passengerCnt;
    private int passengerIndex;

    public TransferFrame(String ip, String port){
        try {
            Socket socket = new Socket(ip, Integer.parseInt(port));
            pw = new PrintWriter(socket.getOutputStream());
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        cards = new JPanel(new CardLayout());

        this.setSize(500, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        makeServiceSelector();
        makeSearchPanel();
        makeReservePanel();
        makeFinalizePanel();

        getContentPane().add(cards);

        this.pack();
        this.setLayout(null);

        serviceSelection.setVisible(true);

    }

    private void makeFinalizePanel(){
        finzalizePanel = new JPanel();
        finzalizePanel.setBackground(Color.lightGray);

        cards.add(finzalizePanel, "finalizep");

        final JTextField token = new JTextField(50);

        finzalizePanel.add(token);


        JButton reserve = new JButton("Finalize");

        final JTable flightTable = new JTable();
        final DefaultTableModel dtm = new DefaultTableModel(0, 0);
        dtm.setColumnIdentifiers(new String[] {"FirstName", "LastName", "RefrenceCode", "TicketNum", "Origin"
                , "Destination", "Airline", "FlightNum", "SeatClass", "DepTime", "ArrivalTime", "Model"});
        flightTable.setModel(dtm);

        final JScrollPane sp = new JScrollPane(flightTable);

        finzalizePanel.add(sp);

        reserve.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                flightTable.removeAll();
                String toSend = "finalize ";

                toSend += token.getText();

                pw.write(toSend + "\n");
                pw.flush();

                dtm.setRowCount(0);

                try {
                    String readStr;
                    String[] parts;
                    readStr = br.readLine();
                    parts = readStr.split(" ");
                    dtm.addRow(parts);
                    while(br.ready()){
                        readStr = br.readLine();
                        parts = readStr.split(" ");
                        dtm.addRow(parts);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        finzalizePanel.add(reserve);
    }

    private void makeReservePanel(){
        reservePanel = new JPanel();
        reservePanel.setBackground(Color.lightGray);

        cards.add(reservePanel, "reservep");

        DefaultListModel<String> ls = new DefaultListModel<String>();
        ls.addElement("THR");
        ls.addElement("MHD");
        ls.addElement("IFN");

        final JList<String> from = new JList<String>(ls);
        final JList<String> to = new JList<String>(ls);

        reservePanel.add(from);
        reservePanel.add(to);

        String[] dates = {"05Feb", "06Feb", "07Feb", "08Feb", "09Feb", "10Feb"};
        final JComboBox<String> date = new JComboBox<String>(dates);

        reservePanel.add(date);

        ls = new DefaultListModel<String>();

        ls.addElement("IR");
        ls.addElement("W5");

        final JList<String> airline = new JList<String>(ls);

        reservePanel.add(airline);

        final JTextField flightNum = new JTextField(5);

        reservePanel.add(flightNum);

        final JTextField seatClass = new JTextField(1);

        reservePanel.add(seatClass);

        final JTextField adultCnt = new JTextField(1);
        final JTextField childCnt = new JTextField(1);
        final JTextField infantCnt = new JTextField(1);

        reservePanel.add(adultCnt);
        reservePanel.add(childCnt);
        reservePanel.add(infantCnt);

        final JTextField fs = new JTextField(8);
        fs.setVisible(false);
        final JTextField sn = new JTextField(8);
        sn.setVisible(false);
        final JTextField natId = new JTextField(8);
        natId.setVisible(false);

        reservePanel.add(fs);
        reservePanel.add(sn);
        reservePanel.add(natId);

        final JButton sendPassenger = new JButton("Send Cred");
        sendPassenger.setVisible(false);

        reservePanel.add(sendPassenger);

        final JLabel shownToken = new JLabel();

        reservePanel.add(shownToken, BorderLayout.CENTER);

        final JButton reserve = new JButton("Reserve");

        reserve.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                String toSend = "reserve ";

                toSend += from.getSelectedValue() + " ";
                toSend += to.getSelectedValue() + " ";
                toSend += date.getItemAt(date.getSelectedIndex()) + " ";
                toSend += airline.getSelectedValue() + " ";
                toSend += flightNum.getText() + " ";
                toSend += seatClass.getText() + " ";
                toSend += adultCnt.getText() + " ";
                toSend += childCnt.getText() + " ";
                toSend += infantCnt.getText() + " ";
                pw.write(toSend + "\n");
                pw.flush();

                passengerIndex = 0;
                passengerCnt = Integer.parseInt(adultCnt.getText()) + Integer.parseInt(childCnt.getText()) + Integer.parseInt(infantCnt.getText());

                fs.setVisible(true);
                sn.setVisible(true);
                natId.setVisible(true);
                sendPassenger.setVisible(true);


            }
        });

        sendPassenger.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {

                String cred;
                System.out.println(passengerCnt);
                cred = fs.getText() + " " + sn.getText() + " " + natId.getText() + "\n";
                System.out.println(cred);
                pw.write(cred);
                pw.flush();

                passengerIndex ++;

                if (passengerIndex == passengerCnt) {
                    fs.setVisible(false);
                    sn.setVisible(false);
                    natId.setVisible(false);
                    sendPassenger.setVisible(false);
                    try {
                        String token = br.readLine();
                        System.out.println(token);
                        shownToken.setText(token);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        reservePanel.add(reserve);
    }

    private void makeSearchPanel(){
        searchPanel = new JPanel();
        searchPanel.setBackground(Color.lightGray);

        cards.add(searchPanel, "searchp");

        DefaultListModel<String> ls = new DefaultListModel<String>();
        ls.addElement("THR");
        ls.addElement("MHD");
        ls.addElement("IFN");

        final JList<String> from = new JList<String>(ls);
        final JList<String> to = new JList<String>(ls);

        searchPanel.add(from);
        searchPanel.add(to);

        String[] dates = {"05Feb", "06Feb", "07Feb", "08Feb", "09Feb", "10Feb"};
        final JComboBox<String> date = new JComboBox<String>(dates);

        searchPanel.add(date);

        final JTextField adultCnt = new JTextField(1);
        final JTextField childCnt = new JTextField(1);
        final JTextField infantCnt = new JTextField(1);

        searchPanel.add(adultCnt);
        searchPanel.add(childCnt);
        searchPanel.add(infantCnt);

        final JTable flightTable = new JTable();
        final DefaultTableModel dtm = new DefaultTableModel(0, 0);
        dtm.setColumnIdentifiers(new String[] {"Airline", "FlightNumber", "DepTime", "ArvTime", "Plane Model", "ClassCosts"});
        flightTable.setModel(dtm);

        final JScrollPane sp = new JScrollPane(flightTable);

        searchPanel.add(sp);

        JButton search = new JButton("Search");

        search.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                String toSend = "search ";

                toSend += from.getSelectedValue() + " ";
                toSend += to.getSelectedValue() + " ";
                toSend += date.getItemAt(date.getSelectedIndex()) + " ";
                toSend += adultCnt.getText() + " ";
                toSend += childCnt.getText() + " ";
                toSend += infantCnt.getText() + " ";
                pw.write(toSend + "\n");
                pw.flush();

                dtm.setRowCount(0);
                try {
                    String readStr;
                    String classCosts;
                    String[] parts;
                    readStr = br.readLine();
                    readStr = readStr.replace("Flight: ", "").replace("Departure: ", "")
                            .replace("Arrival: ", "").replace("Airplane: ", "");
                    parts = readStr.split(" ");
                    classCosts = "";
                    while(!(readStr = br.readLine()).equals("***")) {
                        classCosts += readStr;
                    }
                    String[] newRow = Arrays.copyOf(parts, parts.length+1);
                    newRow[parts.length] = classCosts;
                    dtm.addRow(newRow);
                    while(br.ready()){
                        readStr = br.readLine();
                        readStr = readStr.replace("Flight: ", "").replace("Departure: ", "")
                                .replace("Arrival: ", "").replace("Airplane: ", "");
                        parts = readStr.split(" ");
                        classCosts = "";
                        while(!(readStr = br.readLine()).equals("***") && br.ready()) {
                            classCosts += readStr;
                        }
                        newRow = Arrays.copyOf(parts, parts.length+1);
                        newRow[parts.length] = classCosts;

                        dtm.addRow(newRow);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

        searchPanel.add(search);
    }

    private void makeServiceSelector(){
        serviceSelection = new JPanel();
        serviceSelection.setBackground(Color.black);

        getContentPane().add(serviceSelection, BorderLayout.PAGE_START);

        JButton selectSearch = new JButton("Search");
        JButton selectReserve = new JButton("Reserve");
        JButton selectFinalize = new JButton("Finalize");

        serviceSelection.add(selectSearch);
        serviceSelection.add(selectReserve);
        serviceSelection.add(selectFinalize);

        selectSearch.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "searchp");
            }
        });

        selectReserve.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "reservep");
            }
        });

        selectFinalize.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                CardLayout cl = (CardLayout) (cards.getLayout());
                cl.show(cards, "finalizep");
            }
        });
    }
}
