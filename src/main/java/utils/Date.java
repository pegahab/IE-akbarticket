package utils;

import java.util.Map;

/**
 * Created by bornarz on 2/9/17.
 */
public class Date {
    private String day;
    private String month;
    private String year;

    public Date(String stringInput) {
        if (!stringInput.contains("-")) {
            this.day = stringInput.substring(0, 2);
            this.month = stringInput.substring(2);
            this.year = "2017";
        }else{
            String[] parts = stringInput.split("-");
            if(!parts[0].equals("2017")){
                System.out.println("please chose from current year");
            }
            this.year = parts[0];
            this.month = numberToMonth(parts[1]);
            this.day = parts[2];
        }
    }

    public String toDB(){
        return year + '-' + monthToNumber() + '-' + day;
    }

    private String numberToMonth(String mon){
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Dec"};
        return months[Integer.parseInt(mon) - 1];
    }

    private String monthToNumber(){
        String[] months = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Dec"};
        for (int i = 0; i < months.length; i++) {
            if (month.equals(months[i])){
                i++;
                if (i > 10)
                    return String.valueOf(i);
                else
                    return "0" + i;
            }
        }
        return "1";
    }

    public String getDay() {
        return day;
    }

    public String getMonth() {
        return month;
    }

    public String toString(){
        return day + month;
    }

    public boolean equals (Object date){
        Date targetDate;
        if (date instanceof Date)
             targetDate = (Date) date;
        else
            return false;
        if(targetDate.day.equals(this.day) && targetDate.month.equals(this.month)){
            return true;
        }
        return false;
    }
}
