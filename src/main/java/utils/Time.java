package utils;

/**
 * Created by bornarz on 2/9/17.
 */
public class Time {
    private String hour;
    private String min;

    public Time(String militaryFormattedTime) {
        this.hour = militaryFormattedTime.substring(0, 2);
        this.min = militaryFormattedTime.substring(2, 4);
    }

    public String toDB(){
        return hour + ":" + min + ":" + "00";
    }

    public String toMilitary(){
        return hour + min;
    }

    public String toString() {
        return hour + ":" + min;
    }

    public String getHour() {
        return hour;
    }

    public String getMin() {
        return min;
    }

    public boolean isWithinThirtyMins(Time now) {
        return Integer.parseInt(now.hour) * 60 + Integer.parseInt(now.min) - (Integer.parseInt(hour) * 60 + Integer.parseInt(min)) < 30;
    }
}
