package Server.Services;


import Server.Common.Admin;
import Server.Common.CommonUser;
import Server.Common.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.*;


/**
 * Created by pegahabdi on 5/26/17.
 */
@Path("/usermanagement")
public class UserManager {
    @Context
    private HttpServletRequest request;

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response login(LoginInfo loginInfo) {

        //db search
        //you should split loginInfo sajad has write it this way
        //also check for admin? no they are the same :))
        User result = null;
        try {
            result = User.authenticate(loginInfo.getUsername(), loginInfo.getPassword());
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("Cannot connect the database!", e);

        }
        if (result instanceof CommonUser) {
            {
                HttpSession session = request.getSession();
                session.setAttribute("username", loginInfo.username);
            }

        }
        return Response.status(200).entity(result).build();
    }
}
