package Server.Services;

import Server.Common.*;
import Server.Common.DAOs.FlightDAO;
import Server.Common.DAOs.SeatClassDAO;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by bornarz on 4/20/17.
 */

@Path("/reserve")
public class ReserveService {
    final Logger logger = Logger.getLogger(String.valueOf(ReserveService.class));

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response reserve(ReserveInputs reserveInputs) {
        try {
            Flight flight = FlightDAO.getInstance().getFlightByHash(reserveInputs.getFlightHash());
            SeatClass seatClass = flight.findSeatByName(reserveInputs.getClassName());
            System.out.println(seatClass.getAdultPrice() + "-" + seatClass.getChildPrice() + "-" +seatClass.getInfantPrice());
            logger.info("TMPRES " + flight.getFlightNum() + " " + seatClass.getAdultPrice() * reserveInputs.getAdults()
                    + " " + seatClass.getChildPrice() * reserveInputs.getChilds()
                    + " " + seatClass.getInfantPrice() * reserveInputs.getInfants());

            String[] results = Transmitter.getInstance().requestPRICE(seatClass.getOrigCode(), seatClass.getDestCode()
                    , seatClass.getAirlineCode(), seatClass.getClassName());

            System.out.println(Integer.parseInt(results[0]) + "-" + Integer.parseInt(results[1]) + "-" + Integer.parseInt(results[2]));

            int totalPrice = Integer.parseInt(results[0]) * reserveInputs.getAdults() +
                    Integer.parseInt(results[1]) * reserveInputs.getChilds() +
                    Integer.parseInt(results[2]) * reserveInputs.getInfants();

            if (totalPrice != Integer.parseInt(reserveInputs.getCost())){
                SeatClassDAO.getInstance().update_seatClass(seatClass.getOrigCode(), seatClass.getDestCode(), seatClass.getAirlineCode(), seatClass.getClassName());
            }
            seatClass.updatePrices();
            System.out.println(totalPrice);
            System.out.println(Integer.parseInt(reserveInputs.getCost()));
            ReserveBean reserveBean = new ReserveBean(flight, reserveInputs.getAdults(), reserveInputs.getChilds()
                    , reserveInputs.getInfants(), seatClass, totalPrice == Integer.parseInt(reserveInputs.getCost()));
            return Response.status(200).entity(reserveBean).build();

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(500).entity(new Error(e)).build();
        }
    }
}
