package Server.Services;

/**
 * Created by bornarz on 4/20/17.
 */
public class SearchInputs {
    private String src;
    private String dst;
    private String date;
    private String adults;
    private String childs;
    private String infants;

    public void setSrc(String src) {
        this.src = src;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setAdults(String adults) {
        this.adults = adults;
    }

    public void setChilds(String childs) {
        this.childs = childs;
    }

    public void setInfants(String infants) {
        this.infants = infants;
    }

    public String getSrc() {

        return src;
    }

    public String getDst() {
        return dst;
    }

    public String getDate() {
        return date;
    }

    public String getAdults() {
        return adults;
    }

    public String getChilds() {
        return childs;
    }

    public String getInfants() {
        return infants;
    }
}
