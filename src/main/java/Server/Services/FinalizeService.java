package Server.Services;

import Server.Common.*;
import Server.Common.DAOs.FlightDAO;
import Server.Common.DAOs.ReserveDAO;
import Server.Queries.FinalizeQuery;
import Server.Queries.ReserveQuery;
import org.apache.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Vector;

/**
 * Created by bornarz on 4/20/17.
 */
@Path("/finalize")
public class FinalizeService {
    final Logger logger = Logger.getLogger(String.valueOf(FinalizeService.class));

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response finalize(FinalizeInputs finalizeInputs) {
        try {
            String resPK = reserveTemp(finalizeInputs);

            Vector<Ticket> tickets = finalizeReserve(resPK);

            return Response.status(200).entity(tickets).build();


        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(500).entity(new Error(e)).build();

        }
    }

    private Passenger.PassengerType getType(String t){
        if (t.equals("بزرگسال"))
            return Passenger.PassengerType.ADULT;
        if (t.equals("خردسال"))
            return Passenger.PassengerType.CHILD;
        if (t.equals("نوزاد"))
            return Passenger.PassengerType.INFANT;
        return Passenger.PassengerType.ADULT;
    }

    private String reserveTemp(FinalizeInputs finalizeInputs) throws Exception {
        int numOfPassengers = Integer.parseInt(finalizeInputs.getAdults()) +
                Integer.parseInt(finalizeInputs.getChilds()) +
                Integer.parseInt(finalizeInputs.getInfants());
        Flight flight = FlightDAO.getInstance().getFlightByHash(finalizeInputs.getFlightHash());

        ReserveQuery reserveQuery = new ReserveQuery(flight, finalizeInputs.getClassName(),
                Integer.parseInt(finalizeInputs.getAdults()), Integer.parseInt(finalizeInputs.getChilds()),
                Integer.parseInt(finalizeInputs.getInfants()), finalizeInputs.getPassengers());

        Reservation reservation = reserveQuery.handle();


        return reservation.getToken();
    }

    private Vector<Ticket> finalizeReserve(String reserveToken) throws Exception {
        Reservation reservation = ReserveDAO.getInstance().getReservation(reserveToken);
        FinalizeQuery finalizeQuery = new FinalizeQuery(reservation);
        logger.info("FINRES " + reservation.getToken() + " " + reservation.hashCode() + " " + reservation.getPrice());

        return finalizeQuery.handle();
    }
}
