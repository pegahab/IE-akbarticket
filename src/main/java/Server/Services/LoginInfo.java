package Server.Services;

/**
 * Created by pegahabdi on 5/26/17.
 */

public class LoginInfo {
    String username;
    String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
