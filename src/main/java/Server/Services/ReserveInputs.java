package Server.Services;

/**
 * Created by bornarz on 4/20/17.
 */
public class ReserveInputs {
    private String flightHash;
    private String className;
    private String cost;
    private String adults;
    private String childs;
    private String infants;

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setFlightHash(String flightHash) {
        this.flightHash = flightHash;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setAdults(String adults) {
        this.adults = adults;
    }

    public void setChilds(String childs) {
        this.childs = childs;
    }

    public void setInfants(String infants) {
        this.infants = infants;
    }

    public String getCost() {
        return cost;
    }

    public String getFlightHash() {

        return flightHash;
    }

    public String getClassName() {
        return className;
    }

    public int getAdults() {
        return Integer.parseInt(adults);
    }

    public int getChilds() {
        return Integer.parseInt(childs);
    }

    public int getInfants() {
        return Integer.parseInt(infants);
    }
}
