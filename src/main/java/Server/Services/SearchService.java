package Server.Services;

import Server.Common.Flight;
import Server.Queries.SearchQuery;
import org.apache.log4j.Logger;
import utils.Date;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Vector;

/**
 * Created by bornarz on 4/20/17.
 */
@Path("/search")
public class SearchService {
    final Logger logger = Logger.getLogger(String.valueOf(SearchService.class));

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response search(SearchInputs searchInputs) {
        SearchQuery query = new SearchQuery(searchInputs.getSrc(), searchInputs.getDst(),
                new Date(searchInputs.getDate()), Integer.parseInt(searchInputs.getAdults())
                , Integer.parseInt(searchInputs.getChilds()), Integer.parseInt(searchInputs.getInfants()));
        Vector<Flight> flights;
        try {
            flights = query.handle();
            logger.info("SRCH " + searchInputs.getSrc() + " " + searchInputs.getDst() + " " + searchInputs.getDate());
            return Response.status(200).entity(flights).build();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Response.status(500).entity(new Error(e)).build();
        }
    }

    // This method is called if HTML is request
    @GET
    @Produces(MediaType.TEXT_HTML)
    public String sayHtmlHello() {
        return "<html> " + "<title>" + "Hello Jersey" + "</title>"
                + "<body><h1>" + "Hello Jersey" + "</body></h1>" + "</html> ";
    }
}
