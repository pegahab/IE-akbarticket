package Server.Services;

import Server.Common.Passenger;

import java.util.Vector;

/**
 * Created by bornarz on 4/20/17.
 */
public class FinalizeInputs {
    private String adults;
    private String childs;
    private String infants;
    private String flightHash;
    private String className;
    private Vector<Passenger> passengers;

    public void setAdults(String adults) {
        this.adults = adults;
    }

    public void setChilds(String childs) {
        this.childs = childs;
    }

    public void setInfants(String infants) {
        this.infants = infants;
    }

    public void setFlightHash(String flightHash) {
        this.flightHash = flightHash;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public void setPassengers(Vector<Passenger> passengers) {
        this.passengers = passengers;
    }

    public String getAdults() {

        return adults;
    }

    public String getChilds() {
        return childs;
    }

    public String getInfants() {
        return infants;
    }

    public String getFlightHash() {
        return flightHash;
    }

    public String getClassName() {
        return className;
    }

    public Vector<Passenger> getPassengers() {
        return passengers;
    }
}
