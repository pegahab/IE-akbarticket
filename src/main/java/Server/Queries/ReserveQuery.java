package Server.Queries;

import Server.Common.*;
import Server.Common.DAOs.ReserveDAO;
import org.apache.log4j.Logger;

import java.util.Vector;

/**
 * Created by bornarz on 2/9/17.
 */
public class ReserveQuery {

    final Logger logger = Logger.getLogger(String.valueOf(ReserveQuery.class));

    private Flight flight;
    private String seatClass;
    private int adultCnt;
    private int childCnt;
    private int infantCnt;
    private Vector<Passenger> passengers;

    public ReserveQuery(Flight flight, String seatClass, int adultCnt, int childCnt, int infantCnt, Vector<Passenger> passengers) {



        this.flight = flight;
        this.seatClass = seatClass;
        this.adultCnt = adultCnt;
        this.childCnt = childCnt;
        this.infantCnt = infantCnt;
        this.passengers = passengers;
    }

    public Reservation handle() throws Exception {
        String token;
        String[] tokenParts;
        int price;
        int passengerCount = adultCnt + childCnt + infantCnt;

        Vector<String> tokenData;

        if(!flight.hasSeats(seatClass, passengerCount)){
            throw new Exception("not enough Seats");
        }


        tokenData = Transmitter.getInstance().requestRES(flight.getOrigCode(),flight.getDestCode()
                , flight.getFlightDate(), flight.getAirlineCode(), flight.getFlightNum()
                , seatClass, adultCnt, childCnt, infantCnt, passengers);

        if(tokenData.size() == 0 || tokenData.get(0) == null)
            throw new Exception("server Not Responding");
        tokenParts = tokenData.get(0).split(" ");


        price = Integer.parseInt(tokenParts[1])*adultCnt + Integer.parseInt(tokenParts[2])*childCnt + Integer.parseInt(tokenParts[3])*infantCnt;
        token = tokenParts[0];

        logger.info("RES " + flight.getFlightNum() + " " + token + " " + adultCnt + " " + childCnt + " " + infantCnt);

        Reservation reservation = new Reservation(token, passengers, flight, seatClass, price);
        ReserveDAO.getInstance().addReservation(reservation);


        return reservation;
    }
}
