package Server.Queries;

import Server.Common.*;
import Server.Common.DAOs.ReserveDAO;
import Server.Common.DAOs.TicketDAO;
import org.apache.log4j.Logger;

import java.util.Vector;

/**
 * Created by bornarz on 2/9/17.
 */
public class FinalizeQuery {
    final Logger logger = Logger.getLogger(String.valueOf(FinalizeQuery.class));


    private Reservation reservation;


    public FinalizeQuery(Reservation reservation) {
        this.reservation = reservation;
    }

    public Vector<Ticket> handle() throws Exception {
        String refCode;
        Vector<String> responses = Transmitter.getInstance().requestFIN(reservation.getToken());
        if(responses.size() == 0 || responses.get(0) == null)
            throw new Exception("server Not Responding");
        refCode = responses.get(0);

        reservation.setRefCode(refCode);
        ReserveDAO.getInstance().updatePassRefCode(reservation, refCode);

        Vector<Passenger> passengers = reservation.getPassengers();

        Vector<Ticket> tickets = new Vector<Ticket>();
        Ticket ticket;

        for(int i = 1 ; i < responses.size() ; i++) {
            ticket = new Ticket(passengers.get(i - 1), reservation.getFlight(), reservation.getFlight().findSeatByName(reservation.getSeatClass()), refCode, responses.get(i));
            tickets.add(ticket);
            logger.info("TICKET " + refCode +  " " + ticket.getTicketNum() + " " + ticket.getPassenger().getNid()
                    + " " + ticket.getPassenger().getType() + " " + ticket.getSeatClass().getPriceByType(ticket.getPassenger().getType()));

            TicketDAO.getInstance().saveTicket(ticket);
        }

        return tickets;
    }
}
