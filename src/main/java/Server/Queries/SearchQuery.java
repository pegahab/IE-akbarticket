package Server.Queries;

import Server.Common.Flight;
import Server.Common.DAOs.FlightDAO;
import utils.Date;

import java.util.Vector;

/**
 * Created by bornarz on 2/9/17.
 */
public class SearchQuery {
    private String origCode;
    private String destCode;
    private Date flightDate;
    private int adultCnt;
    private int childCnt;
    private int infantCnt;

    public SearchQuery(String params) {
        String[] parameters = params.split(" ");
        this.origCode = parameters[0];
        this.destCode = parameters[1];

        this.flightDate = new Date(parameters[2]);
        this.adultCnt = Integer.parseInt(parameters[3]);
        this.childCnt = Integer.parseInt(parameters[4]);
        this.infantCnt = Integer.parseInt(parameters[5]);
    }

    public SearchQuery(String origCode, String destCode, Date flightDate, int adultCnt, int childCnt, int infantCnt) {
        this.origCode = origCode;
        this.destCode = destCode;
        this.flightDate = flightDate;
        this.adultCnt = adultCnt;
        this.childCnt = childCnt;
        this.infantCnt = infantCnt;
    }

    public Vector<Flight> handle() throws Exception {
        return FlightDAO.getInstance().searchForFlights(origCode, destCode, flightDate.toString());

    }
}
