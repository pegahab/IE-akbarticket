package Server.Common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import utils.Date;
import java.util.Vector;

import static java.lang.Thread.sleep;

/**
 * Created by bornarz on 2/9/17.
 */
public class Transmitter {
    private static Transmitter ourInstance = new Transmitter("178.62.207.47", "8081");

    public static Transmitter getInstance() {
        return ourInstance;
    }

    private static Socket socket;

    private Transmitter(String ip, String port) {
        try {
            socket = new Socket(ip, Integer.parseInt(port));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Vector<String> requestAV(String origCode, String destCode, String date){
        return send("AV " + origCode + " " + destCode + " " + date, true);
    }

    public Vector<String> requestRES(String origCode, String destCode, Date flightDate, String airlineCode
            , String flightNum, String seatClass, int adultCnt, int childCnt, int infantCnt, Vector<Passenger> passengers){

        send("RES " + origCode + " " + destCode + " " + flightDate.toString() + " " + airlineCode
                + " " + flightNum + " " + seatClass + " " + adultCnt + " " + childCnt + " " + infantCnt + "\n", false);

        Vector<String> tokenData = null;
        boolean hasOutput = false;
        int counter = 1;
        for(Passenger passenger : passengers){
            if(counter == passengers.size())
                hasOutput = true;
            counter ++;
            tokenData = send(passenger.toString() + "\n", hasOutput);
        }
        return tokenData;
    }

    public Vector<String> requestFIN(String token){
        return send("FIN " + token, true);
    }

    public String[] requestPRICE(String origCode, String destCode, String airlineCode, String className){
        return send("PRICE " + origCode + " " + destCode + " " + airlineCode + " " + className, true).get(0).split(" ");
    }

    private Vector<String> send(String cmd, boolean expectOutput){
        PrintWriter printer;
        Vector<String> response = new Vector<String>();

        try {
            printer = new PrintWriter(socket.getOutputStream());

            printer.write(cmd);
            printer.flush();

            BufferedReader responseReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String lastRespLine;

            if(expectOutput){
                lastRespLine = responseReader.readLine();
                response.add(lastRespLine);
            }
            while(responseReader.ready()){
                lastRespLine = responseReader.readLine();
                response.add(lastRespLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;

    }
}
