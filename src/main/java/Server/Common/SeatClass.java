package Server.Common;

/**
 * Created by bornarz on 3/1/17.
 */
public class SeatClass {
    private String origCode;
    private String destCode;
    private String className;
    private String airlineCode;
    private String id;
    private int adultPrice;
    private int childPrice;
    private int infantPrice;

    public SeatClass(String origCode, String destCode, String className, String airlineCode, String id) {
        this.origCode = origCode;
        this.destCode = destCode;
        this.className = className;
        this.airlineCode = airlineCode;
        this.id = id;
    }

    public void updatePrices() {
        String[] prices;
        prices = Transmitter.getInstance().requestPRICE(origCode, destCode, airlineCode, className);
        adultPrice = Integer.parseInt(prices[0]);
        childPrice = Integer.parseInt(prices[1]);
        infantPrice = Integer.parseInt(prices[2]);
    }

    public void updatePrices(int adultPrice, int childPrice, int infantPrice) {
        this.adultPrice = adultPrice;
        this.childPrice = childPrice;
        this.infantPrice = infantPrice;
    }

    public int calcTotalPrice(int adult, int child, int infant) {
        updatePrices();
        return adult * adultPrice + child * childPrice + infant * infantPrice;
    }

    public String getPriceByType(Passenger.PassengerType t) {
        if (t.equals(Passenger.PassengerType.ADULT))
            return String.valueOf(adultPrice);
        if (t.equals(Passenger.PassengerType.CHILD))
            return String.valueOf(childPrice);
        if (t.equals(Passenger.PassengerType.INFANT))
            return String.valueOf(infantPrice);
        return String.valueOf(adultPrice);
    }

    public int getAdultPrice() {
        return adultPrice;
    }

    public int getChildPrice() {
        return childPrice;
    }

    public int getInfantPrice() {
        return infantPrice;
    }

    public String getOrigCode() {
        return origCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public String getClassName() {
        return className;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public String getId() {
        return id;
    }

    public boolean isEqualTo(String origCode, String destCode, String className) {
        return origCode.equals(this.origCode) && destCode.equals(this.destCode) && className.equals(this.className);
    }

    @Override
    public String toString() {
        return "SeatClass number :" + id;
    }
}
