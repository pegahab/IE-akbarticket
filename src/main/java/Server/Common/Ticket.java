package Server.Common;

/**
 * Created by bornarz on 2/9/17.
 */
public class Ticket {
    private Passenger passenger;
    private Flight flight;
    private String refCode;
    private String ticketNum;
    private SeatClass seatClass;

    public Ticket(Passenger passenger, Flight flight, SeatClass seatClass, String refCode, String ticketNum) {
        this.passenger = passenger;
        this.flight = flight;
        this.seatClass = seatClass;
        this.refCode = refCode;
        this.ticketNum = ticketNum;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public String getRefCode() {
        return refCode;
    }

    public String getTicketNum() {
        return ticketNum;
    }

    public SeatClass getSeatClass() {
        return seatClass;
    }

    public String getSeatClassName() {
        return seatClass.getClassName();
    }

    public String getPlaneModel(){
        return flight.getAirplaneModel();
    }

    public String getFlightName(){
        return flight.getAirlineCode() + " " + flight.getFlightNum();
    }

    public String getPassengerName(){
        return passenger.toString();
    }

    public String getArvTime(){
        return flight.getArvTime().toString();
    }

    public String getDepTime(){
        return flight.getDepTime().toString();
    }

    public String getFlightDate(){
        return flight.getFlightDate().toString();
    }

    public String getOrigCode(){
        return flight.getOrigCode();
    }

    public String getDestCode(){
        return flight.getDestCode();
    }

    @Override
    public String toString() {
        return "Ticket Number :" + ticketNum;
    }
}
