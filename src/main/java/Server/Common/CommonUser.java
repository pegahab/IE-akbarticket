package Server.Common;

import Server.Common.DAOs.TicketDAO;

import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by sajadmovahedi on 5/18/17.
 */
public class CommonUser extends User {
    public CommonUser(String u, String p) {
        username = u;
        password = p;
    }
    public Ticket getTicketInfo(String id) {
        Ticket result = null;
        try {
            result = TicketDAO.getInstance().getTicketByID(id);
        } catch (SQLException e) {
            logger.warning("can't contact the data base");
            e.printStackTrace();
        }
        return result;
    }
    public Vector<Ticket> getAllTickets() {
        Vector<Ticket> T = null;
        logger.warning("this user lacks the privilege to execute this command");
        return T;
    }
}
