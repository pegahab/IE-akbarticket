package Server.Common;

import utils.Pair;

/**
 * Created by bornarz on 3/6/17.
 */
public class  ReserveBean {
    private Flight flight;
    private SeatClass seatClass;
    private boolean costOkay;
    private int adults;
    private int childs;
    private int infants;

    public ReserveBean(Flight flight, int adults, int childs, int infants, SeatClass seatClass, boolean costOkay) {
        this.flight = flight;
        this.adults = adults;
        this.childs = childs;
        this.infants = infants;
        this.seatClass = seatClass;
        this.costOkay = costOkay;
    }

    public boolean isCostOkay() {
        return costOkay;
    }

    public Flight getFlight() {
        return flight;
    }

    public int getAdults() {
        return adults;
    }

    public int getChilds() {
        return childs;
    }

    public int getInfants() {
        return infants;
    }

    public SeatClass getSeatClass() {
        return seatClass;
    }

    public String getClassName(){
        return seatClass.getClassName();
    }

    public String getMaxCap(){
        for (Pair<SeatClass, String> seat : flight.getSeatClassStatuses()){
            if (seat.fst().getClassName().equals(seatClass.getClassName()))
                return seat.snd();
        }
        return "A";
    }

    public String getPlaneModel(){
        return flight.getAirplaneModel();
    }

    public String getOrigCode(){
        return flight.getOrigCode();
    }

    public String getDestCode(){
        return flight.getDestCode();
    }

    public String getFlightCode(){
        return flight.getAirlineCode() + " " + flight.getFlightNum();
    }

    public String getFlightDate(){
        return flight.getFlightDate().toString();
    }

    public int getInfantTotalFee(){
        return infants * seatClass.getInfantPrice();
    }

    public int getChildTotalFee(){
        return childs * seatClass.getChildPrice();
    }

    public int getAdultTotalFee(){
        return adults * seatClass.getAdultPrice();
    }

    public int getInfantFee(){
        return seatClass.getInfantPrice();
    }

    public int getChildsFee(){
        return seatClass.getChildPrice();
    }

    public int getAdultFee(){
        return seatClass.getAdultPrice();
    }

    public int getTotalPrice(){
        return infants * seatClass.getInfantPrice()
                + childs * seatClass.getChildPrice()
                + adults * seatClass.getAdultPrice();
    }

    public String getIcon(int i){
        if(i > adults){
            if (i > childs + adults){
                return "fa-child";
            }
            return "fa-child fa-lg";
        }
        return "fa-male fa-lg";
    }

    public String getType(int i){
        if(i > adults){
            if (i > childs + adults){
                return "نوزاد";
            }
            return "خردسال";
        }
        return "بزرگسال";
    }

    public String getEnType(int i){
        if(i > adults){
            if (i > childs + adults){
                return "infant";
            }
            return "child";
        }
        return "adult";
    }

    public String convertToPersianNumber(int number){
        String englishNum = String.valueOf(number);
        return englishNum.replace("1", "۱").replace("2", "۲").replace("3", "۳").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "۷").replace("8", "۸").replace("9", "۹").replace("0", "۰");
    }

    public String convertToPersianNumber(String number){
        return number.replace("1", "۱").replace("2", "۲").replace("3", "۳").replace("4", "۴").replace("5", "۵")
                .replace("6", "۶").replace("7", "۷").replace("8", "۸").replace("9", "۹").replace("0", "۰");
    }

    public int getNumOfPassengers() {
        return infants + childs + adults;
    }

    @Override
    public String toString() {
        return flight + "-" + seatClass + "-" + costOkay;
    }
}
