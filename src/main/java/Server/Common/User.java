package Server.Common;

import Server.Common.DAOs.UserDAO;

import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Logger;

/**
 * Created by sajadmovahedi on 5/18/17.
 */
public abstract class User {
    protected String username = null;
    protected String password = null;

    final static Logger logger = Logger.getLogger(String.valueOf(User.class));

    public static User authenticate(String u, String p) throws SQLException {
        User user = UserDAO.getInstance().findUser(u, p);
        if(user != null) {
            logger.info("User " + u + " has successfully logged in");
        }
        return user;
    }
    public abstract Vector<Ticket> getAllTickets();
    public abstract Ticket getTicketInfo(String ID);
}
