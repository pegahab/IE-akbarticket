package Server.Common;

import Server.Common.DAOs.SeatClassDAO;
import utils.Date;
import utils.Pair;
import utils.Time;

import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by bornarz on 2/9/17.
 */
public class Flight {
    private String airlineCode;
    private String flightNum;
    private Date flightDate;
    private String airplaneModel;
    private Time depTime;
    private Time arvTime;
    private String origCode;
    private String destCode;
    private String flightHash;
    private Vector<Pair<SeatClass, String>> seatClassStatuses;

    public Flight(String airlineCode, String flightNum, Date flightDate, String origCode, String destCode
            , Time depTime, Time arvTime, String airplaneModel, String flightId) {
        this.airlineCode = airlineCode;
        this.flightNum = flightNum;
        this.flightDate = flightDate;
        this.origCode = origCode;
        this.destCode = destCode;
        this.depTime = depTime;
        this.arvTime = arvTime;
        this.airplaneModel = airplaneModel;
        this.seatClassStatuses = new Vector<Pair<SeatClass, String>>();
        this.flightHash = flightId;
    }

    public void addSeatClass(Pair<SeatClass, String> scs){
        seatClassStatuses.add(scs);
    }

    public boolean isEqualTo(String airlineCode, String flightNum, Date flightDate, String origCode, String destCode) {
        if(airlineCode.equals(this.airlineCode) && flightNum.equals(this.flightNum) && flightDate.equals(this.flightDate)
                && origCode.equals(this.origCode) && destCode.equals(this.destCode)) {
            return true;
        }
        return false;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public String getFlightNum() {
        return flightNum;
    }

    public Date getFlightDate() {
        return flightDate;
    }

    public String getAirplaneModel() {
        return airplaneModel;
    }

    public Time getDepTime() {
        return depTime;
    }

    public Time getArvTime() {
        return arvTime;
    }

    public String getOrigCode() {
        return origCode;
    }

    public String getDestCode() {
        return destCode;
    }

    public String getFlightHash() {
        return this.flightHash;
    }

    public Vector<Pair<SeatClass, String>> getSeatClassStatuses() {
        return seatClassStatuses;
    }

    public Vector<String> getFlightData(int adults, int childs, int infants){
        Vector<String> toBeSent = new Vector<String>();
        String[] prices;
        int totalPrice;
        toBeSent.add("Flight: " + airlineCode + " " + flightNum + " Departure: " + depTime.toString()
                + " Arrival: " + arvTime.toString() + " Airplane: " + airplaneModel);
        for (Pair<SeatClass, String> seatClass:seatClassStatuses) {
            seatClass.fst().updatePrices();
            totalPrice = adults * seatClass.fst().getAdultPrice()
                    + childs * seatClass.fst().getChildPrice()
                    + infants * seatClass.fst().getInfantPrice();
            toBeSent.add("Class: " + seatClass.fst().getClassName() + " Price: " + totalPrice);
        }
        return toBeSent;
    }

    public boolean hasSeats(String seatClassName, int seatCnt){
        for(Pair<SeatClass, String> seatClass : seatClassStatuses){
            if(seatClass.fst().getClassName().equals(seatClassName)){
                if (!seatClass.snd().equals("A") && !seatClass.snd().equals("C"))
                    return (Integer.parseInt(seatClass.snd()) > seatCnt);
                return seatClass.snd().equals("A");
            }
        }
        return false;
    }

    public String getFlightTicketData(String seatClass){
        return  origCode + " " + destCode + " " + airlineCode + " " + flightNum + " " + seatClass +
                " " + depTime.toString() + " " + arvTime.toString() + " " + airplaneModel;
    }

    @Override
    public String toString() {
        return "flight number :" + flightHash;
    }

    public SeatClass findSeatByName(String seatName) throws Exception {
        for (Pair<SeatClass, String> seatClass :
                seatClassStatuses) {
            if (seatClass.fst().getClassName().equals(seatName)){
                return seatClass.fst();
            }
        }
        throw new Exception("no class found");
    }
}
