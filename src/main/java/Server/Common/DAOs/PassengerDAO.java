package Server.Common.DAOs;

import Server.Common.Passenger;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.logging.Logger;

/**
 * Created by sajadmovahedi on 5/20/17.
 */
public class PassengerDAO extends DAO {
    public static PassengerDAO instance = null;
    private Logger logger = Logger.getLogger(String.valueOf(PassengerDAO.class));
    public static PassengerDAO getInstance() {
        if(instance == null)
            instance = new PassengerDAO();
        return instance;
    }

    public Passenger getPassengerByHash(String NID) throws SQLException {
        Passenger result = null;
        ResultSet rs = execSelectQuery("*", "PASSENGERS P", "P.NID = " + "'" + NID + "'");
        if(rs.next()) {
            result = new Passenger(rs.getString("FN"), rs.getString("SN"), rs.getString("NID"), Passenger.PassengerType.ADULT);
            result.setTypeByDBType(rs.getString("TYPE"));
        }
        return result;
    }

    public void savePassenger(Passenger P) throws SQLException {
        ResultSet rs = execSelectQuery("*", "PASSENGERS P", "P.NID = '" + P.getNid() + "'");
        if(!rs.next()) {
            String query = "INSERT INTO PASSENGERS (FN, SN, NID, TYPE, TITLE)" +
                    " VALUES('" + P.getFn() + "', '" + P.getSn() +"', '" + P.getNid() + "' ," + P.getDBType() +
                    ", '" + P.getTitle() + "')";
            execQuery(query);
            logger.info("passenger saved");
        }
        else
            logger.info("passenger info already in the database");
    }
}
