package Server.Common.DAOs;

import Server.Common.Flight;
import Server.Common.Passenger;
import Server.Common.Reservation;
import Server.Common.SeatClass;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class ReserveDAO extends DAO {
    private static ReserveDAO ourInstance = new ReserveDAO();

    public static ReserveDAO getInstance() {
        return ourInstance;
    }

    public static void changeInstance(ReserveDAO reserveDAO){
        ourInstance = reserveDAO;
    }

    private Passenger getPassenger(String nid) throws SQLException {
        ResultSet prs = execSelectQuery("*", "PASSENGERS", "NID='" + nid + "'");
        if (prs.next()) {
            prs.close();
            return new Passenger(prs.getString("TITLE"), prs.getString("FN"),
                    prs.getString("SN"), prs.getString("NID"), getTypeFromDB(prs.getString("TYPE")));
        }
        else
            prs.close();
            return null;
    }

    public void addReservation(Reservation newRes) throws Exception {
        SeatClass sc = SeatClassDAO.getInstance().find_seatClass(newRes.getFlight().getOrigCode(),
                newRes.getFlight().getDestCode(), newRes.getFlight().getAirlineCode(), newRes.getSeatClass());
        String reservationQuery = "INSERT INTO " + "\"RESERVATIONS\"" +
                "(\"TOKEN\", \"FLIGHT_ID\", \"SEAT_CLASS_ID\", \"PRICE\", \"REFCODE\" )" +
                "VALUES ('" + newRes.getToken() + "'," + newRes.getFlight().getFlightHash() + "," + sc.getId() + " ,"
                + Integer.toString(newRes.getPrice()) + ",'" + newRes.getRefCode() + "')";
        execQuery(reservationQuery);

        Reservation res = getReservation(newRes.getToken());
        newRes.setId(res.getId());
        for (Passenger passenger : newRes.getPassengers()) {
            if (getPassenger(passenger.getNid()) == null) {
                String passQuery = "INSERT INTO \"PASSENGERS\" " +
                        "( \"FN\", \"SN\", \"NID\", \"TYPE\" ,\"TITLE\") " +
                        "VALUES ( '" + passenger.getFn() + "', '" + passenger.getSn() + "', '" + passenger.getNid() + "','"
                        + passenger.getDBType() + "', '" + passenger.getTitle() + "')";
                execQuery(passQuery);
            }
            String passResQuery = "INSERT INTO \"RESERVATION_PASSENGERS\" " +
                    "(\"RESERVATION_ID\", \"PASSENGER_NID\" )" +
                    "VALUES (" + res.getId() + ",'" + passenger.getNid() + "')";
            execQuery(passResQuery);
        }
    }

    public void updatePassRefCode(Reservation res, String refCode) throws SQLException {
        execQuery("UPDATE RESERVATIONS SET REFCODE = '" + refCode + "' WHERE ID=" + res.getId());
    }

    private Passenger.PassengerType getTypeFromDB(String type) {
        if (type.equals("2"))
            return Passenger.PassengerType.ADULT;
        else if (type.equals("1"))
            return Passenger.PassengerType.CHILD;
        else
            return Passenger.PassengerType.INFANT;
    }

    public Reservation getReservation(String token) throws Exception {
        Vector<Passenger> passengers = new Vector<Passenger>();
        Flight flight = null;
        String seatClass = null;
        int id = 0;
        String refCode = null;
        int price = 0;
        ResultSet rs = execSelectQuery("*", "RESERVATIONS", "TOKEN='" + token + "'");
        if (rs.next()) {
            flight = FlightDAO.getInstance().getFlightByHash(rs.getString("FLIGHT_ID"));
            seatClass = SeatClassDAO.getInstance().getSeatClassByHash(rs.getString("SEAT_CLASS_ID")).getClassName();
            id = rs.getInt("ID");
            price = rs.getInt("PRICE");
            refCode = rs.getString("REFCODE");
        }
        rs.close();

        ResultSet prs = execSelectQuery("*", "RESERVATION_PASSENGERS rp, PASSENGERS p", "rp.RESERVATION_ID='" + id + "' and rp.PASSENGER_NID = p.NID");
        if (prs.next()) {
            do {
                passengers.add(new Passenger(prs.getString("TITLE"), prs.getString("FN"),
                        prs.getString("SN"), prs.getString("NID"), getTypeFromDB(prs.getString("TYPE"))));
            } while (prs.next());
        }
        prs.close();
        Reservation res = new Reservation(token, passengers, flight, seatClass, price);
        res.setRefCode(refCode);
        res.setId(String.valueOf(id));
        return res;
    }
}