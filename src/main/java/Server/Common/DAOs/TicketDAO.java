package Server.Common.DAOs;

import Server.Common.Ticket;
import Server.Common.Flight;
import Server.Common.Passenger;
import Server.Common.SeatClass;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.List;

/**
 * Created by sajadmovahedi on 5/18/17.
 */
public class TicketDAO extends DAO {
    private static TicketDAO instance = null;

    public static TicketDAO getInstance() {
        if(instance == null)
            instance = new TicketDAO();
        return instance;
    }

    public void saveTicket(Ticket t) throws SQLException {
        String query = "INSERT INTO TICKETS ( PASSENGER_NID, FLIGHT_ID, REF_CODE, TICKET_NUM, SEAT_CLASS_ID ) " +
                "VALUES ( " + t.getPassenger().getNid() + ", " + t.getFlight().getFlightNum() + ", '" + t.getRefCode() + "', '" +
                t.getTicketNum() + "', " + t.getSeatClass().getId() + " )";
        execQuery(query);
        PassengerDAO.getInstance().savePassenger(t.getPassenger());
    }
    public Ticket getTicketByID(String id) throws SQLException {
        Ticket result = null;
        ResultSet rs = execSelectQuery("*", "TICKETS T", "T.TICKET_NUM = '" +
                id + "'");
        if(rs.next()) {
            result = new Ticket(null, null, null, rs.getString("REF_CODE"), rs.getString("TICKET_NUM"));
        }
        return result;
    }
    public Vector<Ticket> getAllTickets() throws SQLException {
        Vector<Ticket> result = new Vector<Ticket>();
        ResultSet rs = execQuery("SELECT * FROM TICKETS");
        while(rs.next()) {
            result.addElement(new Ticket(null, null, null, rs.getString("REF_CODE"), rs.getString("TICKET_NUM")));
        }
        return result;
    }
}
