package Server.Common.DAOs;

import Server.Common.SeatClass;
import Server.Common.Transmitter;
import utils.Time;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by bornarz on 5/5/17.
 */
public class SeatClassDAO extends DAO {
    private static SeatClassDAO ourInstance = new SeatClassDAO();

    public static SeatClassDAO getInstance() {
        return ourInstance;
    }

    public static void changeInstance(SeatClassDAO seatClassDAO){
        ourInstance = seatClassDAO;
    }

    public SeatClass addSeatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        SeatClass seatClass;
        seatClass = update_seatClass(origCode, destCode, airlineCode, className);
        if (seatClass != null)
            return seatClass;
        return addNewSeatClass(origCode, destCode, airlineCode, className);
    }

    public SeatClass update_seatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        SeatClass seatClass = find_seatClass(origCode, destCode, airlineCode, className);
        if (seatClass == null)
            return null;
        seatClass.updatePrices();
        ResultSet rs = execQuery("UPDATE SEAT_CLASSES SET ADULT_PRICE = " + seatClass.getAdultPrice() + ", CHILD_PRICE = "
                + seatClass.getChildPrice() + ", INFANT_PRICE = " + seatClass.getInfantPrice() + ", UPDATED_AT = NOW()" + " WHERE ORIG_CODE = '" + origCode + "' and DEST_CODE = '"
                + destCode + "' and CLASS_NAME = '" + className + "' and AIRLINE_CODE = '" + airlineCode + "' ");
        rs.close();
        return seatClass;
    }

    private SeatClass addNewSeatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        String[] prices = Transmitter.getInstance().requestPRICE(origCode, destCode, airlineCode, className);
        ResultSet rs = execQuery("INSERT INTO SEAT_CLASSES (ORIG_CODE, DEST_CODE, CLASS_NAME, AIRLINE_CODE, ADULT_PRICE, CHILD_PRICE, INFANT_PRICE) VALUES ('"+
                origCode + "', '" + destCode + "', '" + className + "', '" + airlineCode + "', " + prices[0] + ", " + prices[1] + ", " + prices[2] + ")");
        rs.close();
        return find_seatClass(origCode, destCode, airlineCode, className);
    }

    public SeatClass searchForSeatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        ResultSet resultSet = execSelectQuery("*", "SEAT_CLASSES sc", "sc.ORIG_CODE = '" + origCode
                + "' and sc.DEST_CODE = '" + destCode + "' and sc.AIRLINE_CODE = '" + airlineCode + "' and sc.CLASS_NAME = '" + className + "'");
        SeatClass result;
        if(resultSet.next()) {
            Time updated = new Time(resultSet.getString("UPDATED_AT").replace(":", ""));
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
            Time now = new Time(sdf.format(cal.getTime()));

            if (updated.isWithinThirtyMins(now)) {
                result = makeSeatClassFromResultSet(resultSet);
            } else {
                result = addSeatClass(origCode, destCode, airlineCode, className);
            }
        } else{
            result = addNewSeatClass(origCode, destCode, airlineCode, className);
        }
        resultSet.close();
        return result;
    }

    public SeatClass find_seatClass(String origCode, String destCode, String airlineCode, String className) throws SQLException {
        ResultSet rs = execSelectQuery("*", "SEAT_CLASSES sc", "sc.ORIG_CODE = '" + origCode
                + "' and sc.DEST_CODE = '" + destCode + "' and sc.AIRLINE_CODE = '" + airlineCode + "' and sc.CLASS_NAME = '" + className + "'");
        SeatClass result = null;
        if (rs.next())
            result = makeSeatClassFromResultSet(rs);
        rs.close();
        return result;
    }

    public SeatClass getSeatClassByHash(String seatClassId) throws SQLException {
        ResultSet rs = execSelectQuery("*", "SEAT_CLASSES sc", "sc.ID = '" + seatClassId + "'");
        rs.next();
        SeatClass result = makeSeatClassFromResultSet(rs);
        rs.close();
        return result;
    }

    private SeatClass makeSeatClassFromResultSet(ResultSet resultSet) throws SQLException {
        SeatClass seatClass = new SeatClass(resultSet.getString("ORIG_CODE"), resultSet.getString("DEST_CODE")
                , resultSet.getString("CLASS_NAME"), resultSet.getString("AIRLINE_CODE"),  resultSet.getString("ID"));
        seatClass.updatePrices(resultSet.getInt("ADULT_PRICE"), resultSet.getInt("CHILD_PRICE"), resultSet.getInt("INFANT_PRICE"));
        return seatClass;
    }
}
