package Server.Common.DAOs;

import Server.Common.Flight;
import Server.Common.SeatClass;
import Server.Common.Transmitter;
import utils.Date;
import utils.Pair;
import utils.Time;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Vector;

/**
 * Created by bornarz on 5/5/17.
 */
public class FlightDAO extends DAO {
    private static FlightDAO ourInstance = new FlightDAO();

    public static FlightDAO getInstance() {
        return ourInstance;
    }

    public static void changeInstance(FlightDAO flightDAO){
        ourInstance = flightDAO;
    }

    private Flight addFlight(String[] flightParams, String classes) throws SQLException {
        Flight flight;
        flight = update_flight(flightParams[0], flightParams[1], new Date(flightParams[2]), flightParams[3]
                , flightParams[4], new Time(flightParams[5]), new Time(flightParams[6]), flightParams[7], classes);
        if (flight != null)
            return flight;
        return addNewFlight(flightParams[0], flightParams[1], new Date(flightParams[2]), flightParams[3]
                , flightParams[4], new Time(flightParams[5]), new Time(flightParams[6]), flightParams[7], classes);
    }

    private Flight update_flight(String airlineCode, String flightNum, Date flightDate, String origCode
            , String destCode, Time depTime, Time arvTime, String airplaneModel, String classes) throws SQLException {
        Flight flight = find_flight(airlineCode, flightNum, flightDate, origCode, destCode);
        if (flight == null)
            return null;
        ResultSet rs = execQuery("UPDATE FLIGHTS SET DEP_TIME = '" + depTime.toDB() + "', ARV_TIME = '"
                + arvTime.toDB() + "', AIRPLANE_MODEL = '" + airplaneModel + "', UPDATED_AT = NOW()" + " WHERE AIRLINE_CODE = '" + airlineCode
                + "' and FLIGHT_NUM = '" + flightNum + "' and FLIGHT_DATE = '" + flightDate.toDB() + "' and ORIG_CODE = '"
                + origCode + "' and DEST_CODE = '" + destCode + "'");
        rs.close();
        SeatClass sc;
        for (String classStatus :
                classes.split(" ")) {
            sc = SeatClassDAO.getInstance().searchForSeatClass(origCode, destCode, airlineCode, String.valueOf(classStatus.charAt(0)));
            rs = execQuery("UPDATE SEAT_CLASS_STATUSES SET STATUS = '" + classStatus.charAt(1)
                    + "' WHERE FLIGHT_ID = '" + flight.getFlightHash() + "' and SEAT_CLASS_ID = '" + sc.getId() + "'");
            rs.close();
        }
        return flight;
    }

    private Flight addNewFlight(String airlineCode, String flightNum, Date flightDate, String origCode
            , String destCode, Time depTime, Time arvTime, String airplaneModel, String classes) throws SQLException {
        ResultSet rs = execQuery("INSERT INTO FLIGHTS (AIRLINE_CODE, FLIGHT_NUM, FLIGHT_DATE, AIRPLANE_MODEL, DEP_TIME, ARV_TIME, ORIG_CODE, DEST_CODE)" +
                " VALUES ('" + airlineCode + "', '" + flightNum + "', '" + flightDate.toDB() + "', '" + airplaneModel + "', '"
                + depTime.toDB() + "', '" + arvTime.toDB() + "', '" + origCode + "', '" + destCode + "')");
        rs.close();
        rs = execSelectQuery("F.ID", "FLIGHTS F", "F.AIRLINE_CODE = '" + airlineCode
                + "' and F.FLIGHT_NUM = '" + flightNum + "' and F.FLIGHT_DATE = '" + flightDate.toDB()
                + "' and F.ORIG_CODE = '" + origCode + "' and F.DEST_CODE = '" + destCode + "'");
        rs.next();
        String flight_id = rs.getString("ID");
        rs.close();
        SeatClass sc;
        for (String classStatus :
                classes.split(" ")) {
            sc = SeatClassDAO.getInstance().searchForSeatClass(origCode, destCode, airlineCode, String.valueOf(classStatus.charAt(0)));
            rs = execQuery("INSERT INTO SEAT_CLASS_STATUSES (FLIGHT_ID, SEAT_CLASS_ID, STATUS) VALUES (" + flight_id + ", " + sc.getId() + ", '" + classStatus.charAt(1) + "')");
            rs.close();
        }
        return find_flight(airlineCode, flightNum, flightDate, origCode, destCode);
    }

    public Vector<Flight> searchForFlights(String origCode, String destCode, String date) throws SQLException {
        Vector<Flight> results = new Vector<Flight>();
        ResultSet resultSet = execSelectQuery("*", "FLIGHTS F", "F.FLIGHT_DATE = '" + new Date(date).toDB()
                + "' and F.ORIG_CODE = '" + origCode + "' and F.DEST_CODE = '" + destCode + "'");
        if (resultSet.next()) {

            Time updated = new Time(resultSet.getString("UPDATED_AT").replace(":", ""));
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
            Time now = new Time(sdf.format(cal.getTime()));

            if (updated.isWithinThirtyMins(now)) {
                do {
                    results.add(makeFlightFromResultSet(resultSet));
                } while (resultSet.next());
            } else {
                Vector<String> responses = Transmitter.getInstance().requestAV(origCode, destCode, date);
                for (int i = 0; i < responses.size(); i += 2) {
                    results.add(addFlight(responses.get(i).split(" "), responses.get(i + 1)));
                }
            }
        } else {
            Vector<String> responses = Transmitter.getInstance().requestAV(origCode, destCode, date);
            for (int i = 0; i < responses.size(); i += 2) {
                results.add(addFlight(responses.get(i).split(" "), responses.get(i + 1)));
            }
        }
        resultSet.close();
        return results;
    }

    private Flight find_flight(String airlineCode, String flightNum, Date flightDate, String origCode, String destCode) throws SQLException {
        ResultSet resultSet = execSelectQuery("*", "FLIGHTS F", "F.AIRLINE_CODE = '" + airlineCode
                + "' and F.FLIGHT_NUM = '" + flightNum + "' and F.FLIGHT_DATE = '" + flightDate.toDB()
                + "' and F.ORIG_CODE = '" + origCode + "' and F.DEST_CODE = '" + destCode + "'");
        Flight result = null;
        if (resultSet.next())
            result = makeFlightFromResultSet(resultSet);
        resultSet.close();
        return result;
    }

    public Flight getFlightByHash(String flightId) throws SQLException {
        ResultSet resultSet = execSelectQuery("*", "FLIGHTS F", "F.ID = " + flightId);
        resultSet.next();
        Flight result = makeFlightFromResultSet(resultSet);
        resultSet.close();
        return result;
    }

    private Flight makeFlightFromResultSet(ResultSet resultSet) throws SQLException {
        Flight result = new Flight(resultSet.getString("AIRLINE_CODE"), resultSet.getString("FLIGHT_NUM")
                , new Date(resultSet.getString("FLIGHT_DATE")), resultSet.getString("ORIG_CODE")
                , resultSet.getString("DEST_CODE"), new Time(resultSet.getString("DEP_TIME"))
                , new Time(resultSet.getString("ARV_TIME")), resultSet.getString("AIRPLANE_MODEL")
                , resultSet.getString("ID"));
        ResultSet scResultSet = execSelectQuery("*", "SEAT_CLASS_STATUSES scs, SEAT_CLASSES sc",
                "scs.FLIGHT_ID = " + resultSet.getString("ID") + " and sc.ID = scs.SEAT_CLASS_ID");
        while (scResultSet.next()) {
            SeatClass sc = SeatClassDAO.getInstance().searchForSeatClass(scResultSet.getString("ORIG_CODE"), scResultSet.getString("DEST_CODE")
                    , scResultSet.getString("AIRLINE_CODE"), scResultSet.getString("CLASS_NAME"));
            Pair<SeatClass, String> scStatus = new Pair<SeatClass, String>(sc, scResultSet.getString("STATUS"));
            result.addSeatClass(scStatus);
        }
        return result;
    }
}
