package Server.Common.DAOs;

import Server.Common.Admin;
import Server.Common.CommonUser;
import Server.Common.User;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sajadmovahedi on 5/18/17.
 */
public class UserDAO extends DAO {
    private static UserDAO instance = null;

    public static UserDAO getInstance() {
        if(instance == null)
            instance = new UserDAO();
        return instance;
    }

    public User findUser(String username, String password) throws SQLException {
        User result;
        ResultSet rs;
        rs = selectUser(username, password);
        if(rs.next()) {
            if(rs.getBoolean("IS_ADMIN"))
                result = new Admin(username, password);
            else
                result = new CommonUser(username, password);
        }
        else {
            result = null;
        }
        return result;
    }
}
