package Server.Common.DAOs;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.sql.PreparedStatement;

/**
 * Created by bornarz on 5/4/17.
 */
public class DAO {
    private Connection connection;
    public DAO () {
        connection = null;
        try {
            Class.forName("org.hsqldb.jdbcDriver");
            connection = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/IEfirstdb", "SA", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public ResultSet execSelectQuery(String select, String tables, String conditions) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT " + select + " FROM " + tables + " WHERE " + conditions);
        statement.close();
        return resultSet;
    }

    public ResultSet execQuery(String query) throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        statement.close();
        return resultSet;
    }

    public ResultSet selectUser(String username, String password) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        String selectSQL = "SELECT * FROM USERS WHERE USERNAME = ? AND PASSWORD = ?";

        try {
            preparedStatement = connection.prepareStatement(selectSQL);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            rs = preparedStatement.executeQuery();

        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        }
        return rs;
    }
}
