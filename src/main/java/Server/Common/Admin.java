package Server.Common;

import Server.Common.DAOs.TicketDAO;

import java.sql.SQLException;
import java.util.Vector;

/**
 * Created by sajadmovahedi on 5/18/17.
 */
public class Admin extends User {
    public Admin(String u, String p) {
        username = u;
        password = p;
    }

    public Vector<Ticket> getAllTickets() {
        Vector<Ticket> result = null;
        try {
            result = TicketDAO.getInstance().getAllTickets();
        } catch (SQLException e) {
            logger.warning("can't contact the data base");
            e.printStackTrace();
        }
        return result;
    }
    public Ticket getTicketInfo(String ID) {
        Ticket t = null;
        logger.warning("this user lacks the privilege to execute this command");
        return t;
    }
}
