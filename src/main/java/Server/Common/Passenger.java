package Server.Common;

/**
 * Created by bornarz on 2/9/17.
 */


public class Passenger {
    public enum PassengerType { ADULT,CHILD,INFANT }
    private String title;
    private String fn;
    private String sn;
    private String nid;
    private PassengerType type;

    public Passenger() {
    }

    public Passenger(String fn, String sn, String nid, PassengerType type) {
        this.fn = fn;
        this.sn = sn;
        this.nid = nid;
        this.type = type;
        this.title = "";
    }

    public Passenger(String title, String fn, String sn, String nid, PassengerType type) {
        this.title = title;
        this.fn = fn;
        this.sn = sn;
        this.nid = nid;
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public String getFn() {
        return fn;
    }

    public String getSn() {
        return sn;
    }

    public String getNid() {
        return nid;
    }

    public PassengerType getType() {
        return type;
    }

    public String toString() {
        String fPart = title.equals("male")?"Mr":"Ms";
        return fPart + " " + fn + " " + sn;
    }

    public String getCred(){
        return fn + " " + sn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Passenger passenger = (Passenger) o;

        if (fn != null ? !fn.equals(passenger.fn) : passenger.fn != null) return false;
        if (sn != null ? !sn.equals(passenger.sn) : passenger.sn != null) return false;
        if (nid != null ? !nid.equals(passenger.nid) : passenger.nid != null) return false;
        return type == passenger.type;
    }

    @Override
    public int hashCode() {
        int result = fn != null ? fn.hashCode() : 0;
        result = 31 * result + (sn != null ? sn.hashCode() : 0);
        result = 31 * result + (nid != null ? nid.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    public String getDBType() {
        if (this.type == PassengerType.ADULT)
            return "2";
        if (this.type == PassengerType.CHILD)
            return "1";
        else
            return "0";
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public void setFn(String fn) {
        this.fn = fn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public void setNid(String nid) {
        this.nid = nid;
    }

    public void setType(String type) {
        if (type.equals("بزرگسال"))
            this.type = Passenger.PassengerType.ADULT;
        if (type.equals("خردسال"))
             this.type = Passenger.PassengerType.CHILD;
        if (type.equals("نوزاد"))
             this.type = Passenger.PassengerType.INFANT;
    }
    public void setTypeByDBType(String type) {
        if(type.equals("0"))
            this.type = PassengerType.ADULT;
        if(type.equals("1"))
            this.type = PassengerType.CHILD;
        if(type.equals("2"))
            this.type = PassengerType.INFANT;
    }
}
