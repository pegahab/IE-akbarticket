package Server.Common;

import java.util.Vector;

/**
 * Created by bornarz on 2/9/17.
 */
public class Reservation {
    private String token;
    private Vector<Passenger> passengers;
    private Flight flight;
    private String seatClass;
    private String id;

    public void setToken(String token) {
        this.token = token;
    }

    public void setPassengers(Vector<Passenger> passengers) {
        this.passengers = passengers;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    private int price;

    private String refCode;
    public Reservation(String token, Vector<Passenger> passengers, Flight flight, String seatClass, int price) {
        this.token = token;
        this.passengers = passengers;
        this.flight = flight;
        this.seatClass = seatClass;
        this.price = price;
        this.refCode = "";
    }

    public String getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

    public Vector<Passenger> getPassengers() {
        return passengers;
    }

    public Flight getFlight() {
        return flight;
    }

    public String getSeatClass() {
        return seatClass;
    }

    public int getPrice() {
        return price;
    }

    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }
}
