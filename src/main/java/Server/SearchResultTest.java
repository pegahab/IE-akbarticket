package Server;

import java.io.*;
import java.net.Socket;

import static java.lang.Thread.sleep;

/**
 * Created by bornarz on 3/6/17.
 */
public class SearchResultTest {

    public static void main(String[] args) throws IOException, InterruptedException {
        Socket socket;
        socket = new Socket("178.62.207.47", 8081);

        String[] aps = {"THR", "MHD", "IFN"};
        String[] days = {"05Feb", "06Feb", "07Feb", "08Feb", "09Feb"};

        OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
        PrintWriter pw = new PrintWriter(osw);

        InputStreamReader isr = new InputStreamReader(socket.getInputStream());
        BufferedReader br = new BufferedReader(isr);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == j)
                    continue;
                for (int k = 0; k < 5; k++) {
                    pw.println("AV " + aps[i] + " " + aps[j] + " " + days[k] + " 1 1 1");
                    pw.flush();
                    System.out.println("For Search of " + "AV " + aps[i] + " " + aps[j] + " " + days[k] + " 1 1 1" + " : ");
                    System.out.println(br.readLine());
                    while(br.ready()){
                        System.out.println(br.readLine());
                    }
                    System.out.println();
                }
            }
        }
    }


}
