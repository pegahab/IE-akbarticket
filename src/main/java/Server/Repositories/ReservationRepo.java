//package Server.Repositories;
//
//import Server.Common.Reservation;
//
//import java.util.Vector;
//
///**
// * Created by bornarz on 2/28/17.
// */
//public class ReservationRepo {
//    private Vector<Reservation> reservations;
//
//    private static ReservationRepo ourInstance = new ReservationRepo();
//
//    public static ReservationRepo getInstance() {
//        return ourInstance;
//    }
//
//    private ReservationRepo() {
//        reservations = new Vector<Reservation>();
//    }
//
//    public void addReservation(Reservation newRes) throws Exception {
//        for (int i = 0; i < reservations.size(); i++) {
//            if (reservations.get(i).equals(newRes)){
//                throw new Exception("Reservation Not Found");
//            }
//        }
//        reservations.add(newRes);
//    }
//
//    public Reservation getReservation(String token) throws Exception {
//        for (Reservation res :
//                reservations) {
//            if (res.getToken().equals(token)){
//                return res;
//            }
//        }
//        throw new Exception("Reservation Not Found");
//    }
//}
