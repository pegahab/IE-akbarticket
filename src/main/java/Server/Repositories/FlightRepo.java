//package Server.Repositories;
//
//import Server.Common.Flight;
//import Server.Common.Transmitter;
//import utils.Date;
//import utils.Time;
//
//import java.util.Vector;
//
///**
// * Created by bornarz on 2/28/17.
// */
//
//public class FlightRepo {
//    private Vector<Flight> flights;
//
//    private static FlightRepo ourInstance = new FlightRepo();
//
//    public static FlightRepo getInstance() {
//        return ourInstance;
//    }
//
//    private FlightRepo() {
//        flights = new Vector<Flight>();
//    }
//
//    public Flight addFlight(String[] flightParams) {
//        for (Flight flight : flights) {
//            if (flight.isEqualTo(flightParams[0], flightParams[1], new Date(flightParams[2]), flightParams[3]
//                    , flightParams[4])) {
//                return flight;
//            }
//        }
//        Flight newFlight = new Flight(flightParams[0], flightParams[1], new Date(flightParams[2]), flightParams[3]
//                , flightParams[4], new Time(flightParams[5]), new Time(flightParams[6]), flightParams[7], "1");//1 temp for ID
//        flights.add(newFlight);
//
//        return newFlight;
//    }
//
//    public Vector<Flight> searchForFlights(String origCode, String destCode, String date) throws Exception {
//        Vector<String> flightsData = Transmitter.getInstance().requestAV(origCode, destCode, date);
//        if(flightsData.size() == 0 || flightsData.get(0) == null)
//            throw new Exception("server Not Responding");
//
//        Vector<Flight> flights = new Vector<Flight>();
//        String[] flightParams;
//        Flight newFlight;
//
//        if(flightsData.size() == 1)
//            return flights;
//        for(int i = 0 ; i < flightsData.size() ; i += 2){
//            flightParams = flightsData.get(i).split(" ");
//            newFlight = FlightRepo.getInstance().addFlight(flightParams);
////            newFlight.updateClasses(flightsData.get(i+1));
//            flights.add(newFlight);
//        }
//
//        return flights;
//    }
//
//    public Flight find_flight(String airlineCode, String flightNum
//            , Date flightDate, String origCode, String destCode) throws Exception {
//        Vector<Flight> newFlights = searchForFlights(origCode, destCode, flightDate.toString());
//
//        for (Flight flight :
//                newFlights) {
//            if (flight.isEqualTo(airlineCode, flightNum, flightDate, origCode, destCode)){
//                return flight;
//            }
//        }
//        throw new Exception("no such flight");
//    }
//
//    public Flight getFlightByHash(String flightHash) throws Exception {
//        for (Flight flight :
//                flights) {
//            if (flight.hashCode() == Integer.parseInt(flightHash))
//                return flight;
//        }
//        throw new Exception("no such flight");
//    }
//}
