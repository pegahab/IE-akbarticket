//package Server.Repositories;
//
//import Server.Common.SeatClass;
//
//import java.util.Vector;
//
///**
// * Created by bornarz on 3/1/17.
// */
//public class SeatClassRepo {
//    Vector<SeatClass> seatClasses;
//
//    private static SeatClassRepo ourInstance = new SeatClassRepo();
//
//    public static SeatClassRepo getInstance() {
//        return ourInstance;
//    }
//
//    private SeatClassRepo() {
//        seatClasses = new Vector<SeatClass>();
//    }
//
//    public SeatClass addNewSeatClass(String origCode, String destCode, String className, String airlineCode){
//        for (SeatClass seat :
//                seatClasses) {
//            if (seat.isEqualTo(origCode, destCode, className)) {
//                seat.updatePrices();
//                return seat;
//            }
//        }
//        SeatClass newSeatClass = new SeatClass(origCode, destCode, className, airlineCode);
//        newSeatClass.updatePrices();
//        return newSeatClass;
//    }
//}
