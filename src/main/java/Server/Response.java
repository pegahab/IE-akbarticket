package Server;

import java.io.PrintWriter;
import java.util.Vector;

/**
 * Created by bornarz on 2/9/17.
 */
public class Response {
    private Vector<String> responseText;

    public Response(){
        responseText = new Vector<String>();
    }

    public void addResponse(String text){
        responseText.add(text);
    }

    public void addResponse(Vector<String> texts){
        responseText.addAll(texts);
    }

    public Vector<String> getResponseText() {
        return responseText;
    }

    public void respond(PrintWriter clientPW){
        for (String res:responseText) {
            clientPW.write(res + "\n");
            clientPW.flush();
        }
    }
}
