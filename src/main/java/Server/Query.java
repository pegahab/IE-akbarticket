package Server;

/**
 * Created by bornarz on 2/9/17.
 */
public abstract class Query {
    public abstract Response handle() throws Exception;

}
