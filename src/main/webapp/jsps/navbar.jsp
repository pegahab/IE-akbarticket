<%@ page contentType="text/html; charset=UTF-8"%>
<div class="mynavbar">
    <div class="mycol-md-offset-3 mycol-lg-offset-3 mycol-lg-2 mycol-md-2 mycol-sm-6 mycol-xs-6">
        <div class="mypull-left navbar-text">
            <i class="fa fa-user" aria-hidden="true"></i>
            برنا ارضی
            <div class="user-dropdown-content">
                <button type="button" class="dropdown-button">
                    <i class="fa fa-list" aria-hidden="true"></i> بلیت‌های من
                </button>
                <button type="button" class="dropdown-button">
                    <i class="fa fa-sign-out" aria-hidden="true"></i> خروج
                </button>
            </div>
        </div>
    </div>
    <div class="mycol-md-offset-1 mycol-md-3 mycol-lg-offset-2 mycol-lg-2 mycol-sm-6 mycol-xs-6">
        <div class="navbar-logo mynavbar-right">
            <img src="assets/images/LogoBlack.png">
        </div>
        <div class="mypull-right navbar-logo-text">
            تیکِتی
            <i class="fa fa-trademark brand-icon" aria-hidden="true"></i>
        </div>

    </div>
</div>