<jsp:useBean id="reserveBean" scope="request" type="Server.Common.ReserveBean"/>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="headers.jsp">
    <jsp:param name="pageTitle" value="reserve" />
</jsp:include>
<body class="reserve-body">
<%@include file="navbar.jsp"%>
<jsp:include page="breadcrumb.jsp" >
    <jsp:param name="activePage" value="reserve" />
</jsp:include>
<div class="container">
    <div class="col-md-offset-1 col-md-10 col-lg-offset-1 col-lg-10 col-sm-12 col-xs-12">
        <div class="row control-labels">
            <a onclick="history.back(-1)" class="pull-right control-back">
                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                بازگشت به صفحه‌ی نتایج
            </a>
            <div class="pull-left control-time">
                زمان باقیمانده:
                <span class="rem-time">
                    <%-- TODO runable time --%>
					۰۴:۵۹
				</span>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="gray-box">
                <div class="form-label">
                    مشخصات پرواز انتخابی شما
                </div>
                <div class="box round">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 flight-info">
                            <div class="flight-info-item">
                                <i class="fa fa-suitcase" aria-hidden="true"></i>
                                ${reserveBean.className}
                            </div>
                            <div class="flight-info-item">
                                <i class="fa fa-plane" aria-hidden="true"></i>
                                ${reserveBean.planeModel}
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6 flight-info-main">
                            ${reserveBean.origCode}
                            <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                            ${reserveBean.destCode}
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3 flight-info">
                            <div class="flight-info-item">
                                ${reserveBean.flightCode}
                            </div>
                            <div class="flight-info-item">
                                <i class="fa fa-calendar-o" aria-hidden="true"></i>
                                ${reserveBean.flightDate}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-label">
                    صورت حساب سفر شما
                </div>
                <div class="box round">
                    <table class="table table-striped">
                        <thead>
                        <tr>

                            <th>

                            </th>
                            <th>
                                به ازای هر نفر
                            </th>
                            <th>
                                تعداد
                            </th>
                            <th>
                                مجموع
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>
                                بزرگسال
                            <td id="adult-fee" value=${reserveBean.adultFee}>
                                ${reserveBean.convertToPersianNumber(reserveBean.adultFee)} ریال
                            </td>
                            <td id="adult-count" value=${reserveBean.adults}>
                                ${reserveBean.convertToPersianNumber(reserveBean.adults)} نفر
                            </td>
                            <td id="adult-total-price" value=${reserveBean.adultTotalFee}>
                                ${reserveBean.convertToPersianNumber(reserveBean.adultTotalFee)} ریال
                            </td>
                        </tr>
                        <tr>
                            <td>
                                کودک زیر ۱۲ سال
                            </td>
                            <td id="child-fee" value=${reserveBean.childsFee}>
                                ${reserveBean.convertToPersianNumber(reserveBean.childsFee)} ریال
                            </td>
                            <td id="child-count" value=${reserveBean.childs}>
                                ${reserveBean.convertToPersianNumber(reserveBean.childs)} نفر
                            </td>
                            <td id="child-total-price" value=${reserveBean.childTotalFee}>
                                ${reserveBean.convertToPersianNumber(reserveBean.childTotalFee)} ریال
                            </td>
                        </tr>
                        <tr>
                            <td>
                                نوزاد زیر ۲ سال
                            </td>
                            <td id="infant-fee" value=${reserveBean.infantFee}>
                                ${reserveBean.convertToPersianNumber(reserveBean.infantFee)} ریال
                            </td>
                            <td id="infant-count" value=${reserveBean.infants}>
                                ${reserveBean.convertToPersianNumber(reserveBean.infants)} نفر
                            </td>
                            <td id="infant-total-price" value=${reserveBean.infantTotalFee}>
                                ${reserveBean.convertToPersianNumber(reserveBean.infantTotalFee)} ریال
                            </td>
                        </tr>
                        <tr class="res">
                            <td colspan="3">
                                مجموع
                            </td>

                            <td id="total-price" value=${reserveBean.totalPrice}>
                                ${reserveBean.convertToPersianNumber(reserveBean.totalPrice)} ریال
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="box round">
                    <div class="row">
                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <div class="btn btn-block btn-danger" onclick="delPassengerRow()">
                                حذف
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <select name="del-passenger-row" class="selectpicker">
                                <%
                                    for (int i=0; i < reserveBean.getNumOfPassengers(); i++)
                                    {
                                %>
                                <option value="<%=i%>"><%=i%></option>
                                <%
                                    }
                                %>

                            </select>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <input id="maxCap" hidden name="maxCap" value="<%=reserveBean.getMaxCap()%>">
                            <div id="addButton" class="btn btn-block btn-success" onclick="addPassenger()">
                                اضافه
                            </div>
                        </div>
                        <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                            <select name="add-passenger-type" class="selectpicker">
                                <option value="adult">بزرگسال</option>
                                <option value="child">خردسال</option>
                                <option value="infant">نوزاد</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-label">
                    اطلاعات
                </div>
                <div id="alerts">
                </div>
                <div class="row">
                    <form id="desktop-form" action="finalize" method="post" class="hidden-xs hidden-sm">
                        <input hidden name="adults" value="<%=request.getParameter("adults")%>">
                        <input hidden name="childs" value="<%=request.getParameter("childs")%>">
                        <input hidden name="infants" value="<%=request.getParameter("infants")%>">
                        <input hidden name="flightHash" value="<%=request.getParameter("flightHash")%>">
                        <input hidden name="className" value="<%=request.getParameter("className")%>">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="passenger-row-container-des">
                            <%
                                for (int i=0; i < reserveBean.getNumOfPassengers(); i++)
                                {
                            %>
                            <div class="input-passenger-line row" index="<%=i%>" passenger-type="<%=reserveBean.getEnType(i+1)%>">
                                <input hidden name="sex<%=i%>" value="<%=reserveBean.getType(i + 1)%>">
                                <div class="col-md-2 col-lg-2">
                                    <input type="text" class="myinput" name="id<%=i%>" placeholder="شماره ملی" tabindex="<%=i*4+4%>">
                                </div>
                                <div class="col-md-3 col-lg-3 ">
                                    <input type="text" class="myinput" name="sn<%=i%>" placeholder="نام خانوادگی (انگلیسی)" tabindex="<%=i*4+3%>">
                                </div>
                                <div class="col-md-3 col-lg-3">
                                    <input type="text" class="myinput" name="fn<%=i%>" placeholder="نام (انگلیسی)" tabindex="<%=i*4+2%>">
                                </div>
                                <div class="col-md-2 col-lg-2 ">
                                    <select name="title<%=i%>" class="sex-select" tabindex="<%=i*4+1%>">
                                        <option value="male">آقای</option>
                                        <option value="female">خانم</option>
                                    </select>
                                </div>
                                <div class="col-md-2 col-lg-2 ">
                                    <div class="passenger-type">
                                        <i class="fa <%= reserveBean.getIcon(i + 1) %> " aria-hidden="true"></i>
                                        <%= i + " - " + reserveBean.getType(i + 1) %>
                                    </div>
                                </div>
                            </div>
                            <%
                                }
                            %>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <div class="form-btns pull-left">
                                    <button type="button" onclick="checkAndSubmitD()" class="bbtn pull-left">
                                        پرداخت و ثبت نهایی
                                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                    </button>
                                    <a href="#" class="orbtn pull-right"> انصراف</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="mobile-form" action="finalize" method="post" class="visible-xs-block visible-sm-block">
                        <input hidden name="flightHash" value="<%=request.getParameter("flightHash")%>">
                        <input hidden name="adults" value="<%=request.getParameter("adults")%>">
                        <input hidden name="childs" value="<%=request.getParameter("childs")%>">
                        <input hidden name="infants" value="<%=request.getParameter("infants")%>">
                        <input hidden name="className" value="<%=request.getParameter("className")%>">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="passenger-row-container-mob">
                            <%
                                for (int i=0; i < reserveBean.getNumOfPassengers(); i++)
                                {
                            %>
                            <div class="input-passenger-line row" index="<%=i%>"  passenger-type="<%=reserveBean.getEnType(i+1)%>">
                                <input hidden name="sex<%=i%>" value="<%=reserveBean.getType(i + 1)%>">
                                <div class="col-xs-12 col-sm-12">
                                    <div class="passenger-type">
                                        <i class="fa <%= reserveBean.getIcon(i + 1) %> " aria-hidden="true"></i>
                                        <%= i + " - " + reserveBean.getType(i + 1) %>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <select name="title<%=i%>" class="sex-select" tabindex="<%=i*4+1%>">
                                        <option value="male">آقای</option>
                                        <option value="female">خانم</option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <input type="text" name="fn<%=i%>" class="myinput" placeholder="نام (انگلیسی)" tabindex="<%=i*4+2%>">
                                </div>
                                <div class="col-xs-12 col-sm-12">
                                    <input type="text" name="sn<%=i%>" class="myinput" placeholder="نام خانوادگی (انگلیسی)" tabindex="<%=i*4+3%>">
                                </div>

                                <div class="col-xs-12 col-sm-12">
                                    <input type="text" name="id<%=i%>" class="myinput" placeholder="شماره ملی" tabindex="<%=i*4+4%>">
                                </div>
                            </div>
                            <%
                                }
                            %>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                                <div class="form-btns pull-left">
                                    <button type="button" onclick="checkAndSubmitM()" class="bbtn pull-left">
                                        پرداخت و ثبت نهایی
                                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                                    </button>
                                    <a href="#" class="orbtn pull-right"> انصراف</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%@include file="scripts.jsp"%>
<%@include file="footer.jsp"%>
</body>
</html>