<head>
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/bootstrap-select.css">
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/normalize.css">
    <link rel="stylesheet" type="text/css" href="../assets/stylesheets/ticketi.css">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${param.pageTitle}</title>
</head>
