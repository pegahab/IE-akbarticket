<%@ page import="Server.Common.Ticket" %>
<%@ page import="java.util.Vector" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<jsp:include page="headers.jsp">
	<jsp:param name="pageTitle" value="tickets" />
</jsp:include>
<body class="tickets-body">
<%@include file="navbar.jsp"%>
<jsp:include page="breadcrumb.jsp" >
	<jsp:param name="activePage" value="tickets" />
</jsp:include>

<div class="mycontainer">
	<div class="mycol-md-offset-1 mycol-md-10 mycol-lg-offset-1 mycol-lg-10 mycol-sm-12">
		<div class="myrow control-labels">
			<div class="mycol-md-6 mycol-lg-6 mycol-sm-6 mycol-xs-6">
				<a href="#modal-one" class="mypull-left orbtn" id="printall">
					چاپ همه
				</a>
			</div>
			<div class=" mycol-md-6 mycol-lg-6 mycol-sm-6 mycol-xs-6">
				<div class="mylabel mypull-right">
					بلیت های صادرشده
				</div>
			</div>
		</div>
		<br>
		<div class="myrow">
		<%
			for (Ticket ticket :
					(Vector<Ticket>) request.getAttribute("tickets")) {
		%>
			<div class="ticket-box">
				<div class="myrow">
					<div class="mycol-md-6 mycol-lg-6 mycol-sm-12 mycol-xs-12">
							<span class="ticket-box-label mypull-right">
				کد مرجع
							</span>
						<span class="codes">
								<%= ticket.getRefCode() %>
							</span>
					</div>
					<div class="mycol-md-6 mycol-lg-6 mycol-sm-12 mycol-xs-12">
							<span class="ticket-box-label mypull-right">
								شماره بلیت
							</span>
						<span class="codes">
								<%= ticket.getTicketNum() %>
							</span>
					</div>
				</div>
			<hr>
				<div class="myrow">
					<div class="mycol-md-2 mycol-lg-2 mycol-sm-12 mycol-xs-12">
						<div class="black normal-text">
							<%= ticket.getPlaneModel() %>
						</div>
					</div>
					<div class="mycol-md-2 mycol-lg-2 mycol-sm-12 mycol-xs-12">
						<div class="black normal-text">
							<%= ticket.getSeatClassName()%>
						</div>
					</div>
					<div class="mycol-md-4 mycol-lg-4 mycol-sm-12 mycol-xs-12">

						<span class="ticket-box-label">پرواز</span>
						<span class="tickets-info">
									<%= ticket.getFlightName()%>
								</span>
					</div>
					<div class="mycol-md-4 mycol-lg-4 mycol-sm-12 mycol-xs-12">

								<span class="ticket-box-label">
									نام
								</span>
						<span class="tickets-info">
									<%= ticket.getPassengerName() %>
								</span>
					</div>
				</div>
			<hr>
			<div class="myrow">
				<div class="mycol-md-2 mycol-lg-2 mycol-sm-12 mycol-xs-12">
					<div class="ticket-box-label">
						ورود
					</div>
					<br>
					<div class="myinput">
						<%= ticket.getArvTime() %>
					</div>
				</div>
				<div class="mycol-md-2 mycol-lg-2 mycol-sm-12 mycol-xs-12">
					<div class="ticket-box-label">
						خروج
					</div>
					<br>
					<div class="myinput">
						<%= ticket.getDepTime() %>
					</div>
				</div>
				<div class="mycol-md-4 mycol-lg-4 mycol-sm-12 mycol-xs-12">
					<div class="ticket-box-label">
						تاریخ
					</div>
					<br>
					<div class="myinput">
						<%= ticket.getFlightDate() %>
					</div>
				</div>
				<div class="mycol-md-2 mycol-lg-2 mycol-sm-12 mycol-xs-12">
					<div class="ticket-box-label">
						به
					</div>
					<br>
					<div class="myinput">
						<%= ticket.getOrigCode() %>
					</div>
				</div>
				<div class="mycol-md-2 mycol-lg-2 mycol-sm-12 mycol-xs-12">
					<div class="ticket-box-label">
						از
					</div>
					<br>
					<div class="myinput">
						<%= ticket.getDestCode() %>
					</div>
				</div>
			</div>
		</div>
		<%
			}
		%>
	</div>
</div>
</div>
<div class="modal" id="modal-one" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-header">
			<h1> چاپ یا ایمیل بیلت های شما</h1>
		</div>
		<div class="modal-body">
			<a class="printbtn"> جاپ</a>
			<a class="printbtn"> ایمیل</a>
		</div>
		<div class="modal-footer">
			<a href="#close">بستن</a>
		</div>
	</div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>