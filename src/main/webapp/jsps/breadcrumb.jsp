<%@ page contentType="text/html; charset=UTF-8"%>
<div class="mybreadcrumb myrow">
    <div class="mycol-md-offset-1 mycol-md-10 mycol-lg-offset-3 mycol-lg-6 mycol-sm-offset-1 mycol-sm-10 myhidden-xs">
        <div class="mybreadcrumb-item">
            جستجو پرواز
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div class="mybreadcrumb-item" <% if (request.getParameter("activePage").equals("selectFlight")) {out.println("myactive");} %>>
            انتخاب پرواز
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div class="mybreadcrumb-item <% if (request.getParameter("activePage").equals("reserve")) {out.println("myactive");} %>">
            ورود اطلاعات
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </div>
        <div class="mybreadcrumb-item <% if (request.getParameter("activePage").equals("tickets")) {out.println("myactive");} %>">
            دریافت بلیت
        </div>
    </div>
    <div class="mycol-xs-offset-1 mycol-xs-10 myvisible-xs-block">
        <div class="mybreadcrumb-item myactive">
        <% if (request.getParameter("activePage").equals("tickets"))
        {
            out.println("دریافت بلیت");
        }
        else if (request.getParameter("activePage").equals("reserve"))
        {
            out.println("ورود اطلاعات");
        }
        else if (request.getParameter("activePage").equals("selectFlight"))
        {
            out.println("انتخاب پرواز");
        }
        %>
        </div>
    </div>
</div>