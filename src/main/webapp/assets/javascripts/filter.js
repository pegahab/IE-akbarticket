/**
 * Created by amiiigh on 3/13/2017.
 */

// sort price asc or desc

let priceSortTypeSelect = document.getElementsByName("price-filter")[0];
priceSortTypeSelect.onchange = function () {
    let selectedOption = priceSortTypeSelect.options[priceSortTypeSelect.selectedIndex].value;
    let sortedElements;
    let elements = document.getElementsByClassName("flight");
    if (selectedOption == "asc"){
        sortedElements =  sortElements(elements, sortAscending);
    }
    else if (selectedOption == "des"){
        sortedElements =  sortElements(elements, sortDescending);
    }
    let container = document.getElementById("flight-res-container");
    let htmlRes = "";
    for(let el of sortedElements){
        htmlRes += el.outerHTML;
    }
    container.innerHTML = htmlRes;
};

function sortElements( elements, callback ) {
    let elems = [];
    for(let el of elements) {
        elems.push( el );
    }
    return elems.sort(callback);
}

function sortAscending( a, b ) {
    let aValue = a.getAttribute("price");
    let bValue = b.getAttribute("price");
    return aValue < bValue;
}

function sortDescending( a, b ) {
    // let aValue = parseInt(a.firstChild.nodeValue, 10);
    let aValue = a.getAttribute("price");
    let bValue = b.getAttribute("price");
    return aValue > bValue;
}

// filter airline
let allElements = document.getElementsByClassName("flight");
let airlineCodeFilterSelect = document.getElementsByName("airline-code-filter")[0];
airlineCodeFilterSelect.onchange = function () {
    let selectedOption = airlineCodeFilterSelect.options[airlineCodeFilterSelect.selectedIndex].value;
    console.log(selectedOption);
    filter(allElements,selectedOption,"airline");
};

// filter seatclass

let seatClassFilterSelect = document.getElementsByName("seat-class-filter")[0];
seatClassFilterSelect.onchange = function () {
    let selectedOption = seatClassFilterSelect.options[seatClassFilterSelect.selectedIndex].value;
    console.log(selectedOption);
    filter(allElements,selectedOption,"seat-class");
};

function filter(elements,selectedOption,attrName) {
    if (selectedOption == "all"){
        for(let el of elements){
            el.style.display = "block";
        }
    }
    else{
        for(let el of elements){
            el.style.display = "none";
            if (el.getAttribute(attrName) == selectedOption){
                el.style.display = "block";
            }
        }
    }
}