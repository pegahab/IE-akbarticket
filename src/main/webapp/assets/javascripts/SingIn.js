/**
 * Created by pegahabdi on 5/22/17.
 */
$('.error-page').hide(0);

$('.login-button , .no-access').click(function(){
    $('.login').slideUp(500);
    $('.error-page').slideDown(1000);
});

$('.try-again').click(function(){
    $('.error-page').hide(0);
    $('.login').slideDown(1000);
});


(function(){
    var app = angular.module('SignIn', [ ]);

    app.controller("UserController", function($scope, $http, $location){
        localRef = this;
        this.validLogin = true;
        this.userInfo={};

        this.marshal = function(userInfo) {
            return {
                username: userInfo.username,
                password: userInfo.password
            };
        };

        this.login = function(userInfo) {
            localRef = this;
            $http.post('http://localhost:8080/ticketi/ebank/usermanagement/login',
                this.marshal(this.userInfo)).then(
                function(response) {
                    if(response.data == true) {
                        window.location.href='http://localhost:8080/ticketi/login.html';
                    } else {
                        localRef.validLogin = false;
                    }
                }
            );
        };
    });
})();
