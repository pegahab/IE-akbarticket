function createDesktopHTML(passengerIndex,passengerType) {
    return "<div index=\"" + passengerIndex + "\" class=\"input-passenger-line row\" passenger-type=\"" + passengerType + "\">" +
        "<input hidden name=\"sex" + passengerIndex + "\" value=\"" + persianPassengerType(passengerType) + "\">" +
        "<div class=\"col-md-2 col-lg-2\">" +
        "<input type=\"text\" class=\"myinput\" name=\"id" + passengerIndex + "\" placeholder=\"شماره ملی\" tabindex=\"" + passengerIndex * 4 + 4 + "\">" +
        "</div>" +
        "<div class=\"col-md-3 col-lg-3 \">" +
        "<input type=\"text\" class=\"myinput\" name=\"sn" + passengerIndex + "\" placeholder=\"نام خانوادگی (انگلیسی)\" tabindex=\"" + passengerIndex * 4 + 3 + "\">" +
        "</div>" +
        "<div class=\"col-md-3 col-lg-3\">" +
        "<input type=\"text\" class=\"myinput\" name=\"fn" + passengerIndex + "\" placeholder=\"نام (انگلیسی)\" tabindex=\"" + passengerIndex * 4 + 2 + "\">" +
        "</div>" +
        "<div class=\"col-md-2 col-lg-2 \">" +
        "<select name=\"title" + passengerIndex + "\" class=\"sex-select\" tabindex=\"" + passengerIndex * 4 + 1 + "\">" +
        "<option value=\"male\">آقای</option>" +
        "<option value=\"female\">خانم</option>" +
        "</select>" +
        "</div>" +
        "<div class=\"col-md-2 col-lg-2\">" +
        "<div class=\"passenger-type\">" +
        "<i class=\"fa " + getIcon(passengerType) + "\" aria-hidden=\"true\"></i>" +
        passengerIndex + " - " + persianPassengerType(passengerType) +
        "</div> </div> </div>";
}

function createMoblieHTML(passengerIndex,passengerType) {
    return "<div index=\"" + passengerIndex + "\" class=\"input-passenger-line row\" passenger-type=\"" + passengerType + "\">"+
        "<input hidden name=\"sex" + passengerIndex + "\" value=\"" + persianPassengerType(passengerType) + "\">" +
        "<div class=\"col-xs-12 col-sm-12\">"+
        "<div class=\"passenger-type\">"+
        "<i class=\"fa " + getIcon(passengerType) + "\" aria-hidden=\"true\"></i>" +
        passengerIndex + " - " + persianPassengerType(passengerType) +
        "</div>"+
        "</div>"+
        "<div class=\"col-xs-12 col-sm-12\">"+
        "<select name=\"title" + passengerIndex + "\" class=\"sex-select\" tabindex=\"" + passengerIndex * 4 + 1 + "\">"+
        "<option value=\"male\">آقای</option>"+
        "<option value=\"female\">خانم</option>"+
        "</select>"+
        "</div>"+
        "<div class=\"col-xs-12 col-sm-12\">"+
        "<input type=\"text\" name=\"fn" + passengerIndex + "\" class=\"myinput\" placeholder=\"نام (انگلیسی)\" tabindex=\"" + passengerIndex * 4 + 2 + "\">"+
        "</div>"+
        "<div class=\"col-xs-12 col-sm-12\">"+
        "<input type=\"text\" name=\"sn" + passengerIndex + "\" class=\"myinput\" placeholder=\"نام خانوادگی (انگلیسی)\" tabindex=\"" + passengerIndex * 4 + 3 + "\">"+
        "</div>"+
        "<div class=\"col-xs-12 col-sm-12\">"+
        "<input type=\"text\" name=\"id" + passengerIndex + "\" class=\"myinput\" placeholder=\"شماره ملی\" tabindex=\"" + passengerIndex * 4 + 1 + "\">"+
        "</div>"+
        "</div>";

}

function getIcon(passengerType){
    if(passengerType == "adult")
        return "fa-male fa-lg";
    else if (passengerType == "child")
        return "fa-child fa-lg";
    else
        return "fa-child";
}

function getNumOfAdultsPassengers(passengers) {
    let numOfAdults = 0;
    for (let el of passengers){
        if (el.getAttribute("passenger-type") == "adult")
        {
            numOfAdults +=1;
        }
    }
    return numOfAdults;
}

function getNumOfChildPassengers(passengers) {
    let numOfChilds = 0;
    for (let el of passengers){
        if (el.getAttribute("passenger-type") == "child")
        {
            numOfChilds +=1;
        }
    }
    return numOfChilds;
}

function getNumOfInfantPassengers(passengers) {
    let numOfInfants = 0;
    for (let el of passengers){
        if (el.getAttribute("passenger-type") == "infant")
        {
            numOfInfants +=1;
        }
    }
    return numOfInfants;
}

function getAllPassengers(passengersContainerId) {
    return document.getElementById(passengersContainerId).getElementsByClassName("input-passenger-line row");
}

function convertToPersian(english) {
    return english.replace(/0/g, "۰").replace(/1/g, "۱").replace(/2/g, "۲").replace(/3/g, "۳").replace(/4/g, "۴").replace(/5/g, "۵")
        .replace(/6/g, "۶").replace(/7/g, "۷").replace(/8/g, "۸").replace(/9/g, "۹");
}

function convertToEnglish(persian) {
    return persian.replace(/۰/g, "0").replace(/۱/g, "1").replace(/۲/g, "2").replace(/۳/g, "3").replace(/۴/g, "4").replace(/۵/g, "5")
        .replace(/۶/g, "6").replace(/۷/g, "7").replace(/۸/g, "8").replace(/۹/g, "9");
}

function updatePassengerTotalFee(passengerType) {
    let feeElement = document.getElementById(passengerType+"-fee");
    let feeVal = parseInt(feeElement.getAttribute("value"));
    let totalFeeElement = document.getElementById(passengerType+"-total-price");
    let countElement = document.getElementById(passengerType+"-count");
    let countVal = parseInt(countElement.getAttribute("value"));
    let totalFeeVal = totalFeeElement.innerHTML = convertToPersian((countVal * feeVal).toString()) +" "+ "ریال"
    totalFeeElement.setAttribute("value",totalFeeVal.toString());
}

function updateTotalPrice() {
    let adultTotalFee = convertToEnglish(document.getElementById("adult-total-price").getAttribute("value"));
    let childTotalFee = convertToEnglish(document.getElementById("child-total-price").getAttribute("value"));
    let infantTotalFee = convertToEnglish(document.getElementById("infant-total-price").getAttribute("value"));
    let totalPriceElement = document.getElementById("total-price");
    let totalPriceVal = parseInt(adultTotalFee) + parseInt(childTotalFee) + parseInt(infantTotalFee);
    totalPriceElement.setAttribute("value",convertToPersian(totalPriceVal.toString()));
    totalPriceElement.innerHTML = convertToPersian(totalPriceVal.toString()) +" "+ "ریال";
}

function updateTable(action,passengerType) {
    updatePassengerCount(action,passengerType);
    updatePassengerTotalFee(passengerType);
    updateTotalPrice();
}

function updatePassengerCount(action,passengerType) {
    let countElement = document.getElementById(passengerType+"-count");
    let countVal;
    if (action == "add")
        countVal = parseInt(countElement.getAttribute("value"))+ parseInt(1);
    else
        countVal = parseInt(countElement.getAttribute("value"))- parseInt(1);
    countElement.setAttribute("value",countVal.toString());
    countElement.innerHTML = convertToPersian(countVal.toString()) +" "+ "نفر";
}

function persianPassengerType(passengerType) {
    if (passengerType == "adult")
        return "بزرگسال";
    else if(passengerType == "child")
        return "خردسال";
    else
        return "نوزاد";
}

function createMobileInputs(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers) {
    let mobileHTML = "";
    let count = 0;
    for (let i=0;i<numOfAdultPassengers;i++){
        mobileHTML += createMoblieHTML(count,"adult");
        count +=1;
    }
    for (let i=0;i<numOfChildPassengers;i++){
        mobileHTML += createMoblieHTML(count,"child");
        count +=1;
    }
    for (let i=0;i<numOfInfantPassengers;i++){
        mobileHTML += createMoblieHTML(count,"infant");
        count +=1;
    }
    let mobileContainer = document.getElementById("passenger-row-container-mob");
    mobileContainer.innerHTML = mobileHTML;
}

function createDesktopInputs(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers) {
    let desktopHTML = "";
    let count = 0;
    for (let i=0;i<numOfAdultPassengers;i++){
        desktopHTML += createDesktopHTML(count,"adult");
        count +=1;
    }
    for (let i=0;i<numOfChildPassengers;i++){
        desktopHTML += createDesktopHTML(count,"child");
        count +=1;
    }
    for (let i=0;i<numOfInfantPassengers;i++){
        desktopHTML += createDesktopHTML(count,"infant");
        count +=1;
    }
    let desktopContainer = document.getElementById("passenger-row-container-des");
    desktopContainer.innerHTML = desktopHTML;
}

function createInputs(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers) {
    createDesktopInputs(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers);
    createMobileInputs(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers);
}

function updateHiddenInputs(action,inputName) {
    let inputs = document.getElementsByName(inputName);
    for (let input of inputs){
        let inputVal = input.getAttribute("value");
        if (action == "add")
            input.setAttribute("value",(parseInt(inputVal) + parseInt(1)).toString());
        else
            input.setAttribute("value",(parseInt(inputVal) - parseInt(1)).toString());
    }
}

function delPassengerRow() {
    let selectedRow = document.getElementsByName("del-passenger-row")[0].value;
    let desktopPassengers = getAllPassengers("passenger-row-container-des");

    let numOfAdultPassengers = getNumOfAdultsPassengers(desktopPassengers);
    let numOfChildPassengers = getNumOfChildPassengers(desktopPassengers);
    let numOfInfantPassengers = getNumOfInfantPassengers(desktopPassengers);
    let selectedType;

    activateAdd();
    for (let passenger of desktopPassengers){
        if(passenger.getAttribute("index") == selectedRow )
            selectedType = passenger.getAttribute("passenger-type");
    }
    if (selectedType == "adult"){
        numOfAdultPassengers -=1;
        updateHiddenInputs("del","adults");
    }
    else if (selectedType == "child") {
        numOfChildPassengers -= 1;
        updateHiddenInputs("del","childs");
    }
    else {
        numOfInfantPassengers -=1;
        updateHiddenInputs("del","infants");
    }

    createInputs(numOfAdultPassengers,numOfChildPassengers,numOfInfantPassengers);
    updateTable("del",selectedType);
    createDelPassengerList(numOfAdultPassengers+numOfChildPassengers+numOfInfantPassengers);
}

function addPassenger() {
    let desktopPassengers = getAllPassengers("passenger-row-container-des");

    let numOfAdultPassengers = getNumOfAdultsPassengers(desktopPassengers);
    let numOfChildPassengers = getNumOfChildPassengers(desktopPassengers);
    let numOfInfantPassengers = getNumOfInfantPassengers(desktopPassengers);

    let newPassengerType = document.getElementsByName("add-passenger-type")[0].value;

    checkForFull(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers);
    if (newPassengerType == "adult"){
        numOfAdultPassengers +=1;
        updateHiddenInputs("add","adults");
    }
    else if (newPassengerType == "child") {
        numOfChildPassengers += 1;
        updateHiddenInputs("add","childs");
    }
    else {
        numOfInfantPassengers +=1;
        updateHiddenInputs("add","infants");
    }

    createInputs(numOfAdultPassengers,numOfChildPassengers,numOfInfantPassengers);
    updateTable("add",newPassengerType);
    createDelPassengerList(numOfAdultPassengers+numOfChildPassengers+numOfInfantPassengers);
}


function createDelPassengerList(numOfPassengers) {
    let options = "";
    for(let i=0; i<numOfPassengers;i++){
        options +="<option value=\""+i+"\">"+i+"</option>";
    }
    console.log(options);
    document.getElementsByName("del-passenger-row")[0].innerHTML = options;
    $('.selectpicker').selectpicker('refresh');
}

function checkForFull(numOfAdultPassengers, numOfChildPassengers, numOfInfantPassengers){
    let maxCap = document.getElementById("maxCap").value;
    if (maxCap == "A")
        return;
    let maxCapNum = parseInt(maxCap);
    if(maxCapNum - 1 == parseInt(numOfInfantPassengers) + parseInt(numOfChildPassengers) + parseInt(numOfAdultPassengers)){
        document.getElementById("addButton").setAttribute("disabled", "disabled");
        document.getElementById("addButton").removeAttribute("onclick");
    }

}

function activateAdd() {
    document.getElementById("addButton").removeAttribute("disabled");
    document.getElementById("addButton").setAttribute("onclick", "addPassenger()");
}

function checkAndSubmitM() {
    let passengers = document.getElementById("passenger-row-container-mob");
    if(checkInputs(passengers)){
        document.getElementById("mobile-form").submit();
        return true;
    }
    return false;
}

function checkAndSubmitD() {
        let passengers = document.getElementById("passenger-row-container-des");
    if(checkInputs(passengers)) {
        document.getElementById("desktop-form").submit();
        return true;
    }
    return false;
}

function checkInputs(passengers){
    let nameAlert = true;
    let surnameAlert = true;
    let natIdAlert = true;
    let alerts = "";
    let index = 0;
    let name, surname, id;
    for(let passenger of passengers.getElementsByClassName("input-passenger-line row")) {
        id = getAtt(passenger, "id");
        name = getAtt(passenger, "fn");
        surname = getAtt(passenger, "sn");

        if(id == "" || !checkId(id)){
            natIdAlert = false;
        }
        if(surname == "" || !checkName(surname)){
            surnameAlert = false;
        }
        if(name == "" || !checkName(name)){
            nameAlert = false;
        }
        index ++;
    }
    if(!nameAlert)
        alerts += addNameAlert();
    if(!surnameAlert)
        alerts += addSurnameAlert();
    if(!natIdAlert)
        alerts += addNatIdAlert();

    document.getElementById("alerts").innerHTML = alerts;
    return (nameAlert && natIdAlert && surnameAlert);
}

function getAtt(passenger, att) {
    for (let inp of passenger.getElementsByTagName("input")){
        if(inp.getAttribute("name").startsWith(att))
            return inp.value;
    }
}

function addNatIdAlert() {
    return     "                    <div class=\"row alert alert-danger\">\n"+
        "                        لطفا کدهای ملی را دوباره بازبینی کنید!\n"+
        "                    </div>";
}

function addSurnameAlert() {
    return     "                    <div class=\"row alert alert-danger\">\n"+
        "                        لطفا نام های خانوادگی را دوباره بازبینی کنید!\n"+
        "                    </div>";
}

function addNameAlert() {
    return     "                    <div class=\"row alert alert-danger\">\n"+
        "                        لطفا نام ها را دوباره بازبینی کنید!\n"+
        "                    </div>";
}

function checkId(id) {
    return /^\d{10}$/.test(id);
}

function checkName(name) {
    return (/^[a-zA-Z]+$/.test(name));
}