/**
 * Created by amiiigh on 4/19/2017.
 */

var app = angular.module('ticketi',[]);

app.service('Responses', function () {
   this.flights = [{"flightHash":"198447241","flightNum":"452","seatClassStatuses":[{"component1":{"adultPrice":3000,"childPrice":2000,"infantPrice":2000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"Y"},"component2":"8"},{"component1":{"adultPrice":4000,"childPrice":3000,"infantPrice":3000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"M"},"component2":"A"},{"component1":{"adultPrice":5000,"childPrice":4000,"infantPrice":4000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"B"},"component2":"C"}],"airlineCode":"IR","origCode":"THR","depTime":{"min":"40","hour":"17"},"destCode":"MHD","arvTime":{"min":"50","hour":"18"},"airplaneModel":"M80","flightDate":{"month":"Feb","day":"05"}}];
});

app.filter('nonEmpty', function () {
    return function (input) {
        var out = [];
        for (var i = 0; i < input.length; i++) {
            if(!(input[i].component2 === "C")){
                out.push(input[i]);
            }
        }
        return out;
    };
});

app.directive('nidValidator', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, mCtrl) {
            function myValidation(value) {
                var isnum = /^\d+$/.test(value);
                if (value.length == 10 && isnum) {
                    mCtrl.$setValidity('tenLength', true);
                } else {
                    mCtrl.$setValidity('tenLength', false);
                }
                return value;
            }
            mCtrl.$parsers.push(myValidation);
        }
    };
});

app.directive('nameValidator', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attr, mCtrl) {
            function myValidation(value) {
                var isAlphabet = /^[a-zA-Z]*$/.test(value);
                if (value.length > 2 && isAlphabet) {
                    mCtrl.$setValidity('nameLength', true);
                } else {
                    mCtrl.$setValidity('nameLength', false);
                }
                return value;
            }
            mCtrl.$parsers.push(myValidation);
        }
    };
});



app.filter('addAllAirlines', function () {
    return function (input) {
        var out = [];
        var alreadyIn;
        out.push("همه");
        for (var i = 0; i < input.length; i++) {
            alreadyIn = false;
            for (var j = 0; j < out.length ; j++) {
                if (out[j] == input[i].airlineCode){
                    alreadyIn = true;
                    break;
                }
            }
            if(alreadyIn == false) {
                out.push(input[i].airlineCode);
            }
        }
        return out;
    }
});

app.filter('addAllClasses', function () {
    return function (input) {
        var out = [];
        var alreadyIn;
        out.push("همه");
        for (var i = 0; i < input.length; i++) {
            for (var k = 0; k < input[i].seatClassStatuses.length; k++) {
                if(input[i].seatClassStatuses[k].component2 === "C"){
                    continue;
                }
                alreadyIn = false;
                for (var j = 0; j < out.length; j++) {
                    if (out[j] == input[i].seatClassStatuses[k].component1.className) {
                        alreadyIn = true;
                        break;
                    }
                }
                if (alreadyIn == false) {
                    out.push(input[i].seatClassStatuses[k].component1.className);
                }
            }
        }
        return out;
    }
});

app.filter('range', function () {
    return function (input, min, max) {
        min = parseInt(min);
        max = parseInt(max);
        for (var i=min; i<max; i++)
            input.push(i);
        return input;
    };
});

app.filter('completeSeats', function ($rootScope) {
    return function (input) {
        var out = [];
        for (var i = 0; i < input.length; i++){
            for (var j = 0; j < input[i].seatClassStatuses.length; j++){
                out.push(input[i].seatClassStatuses[j]);
                out[out.length-1].depTime = input[i].depTime;
                out[out.length-1].arvTime = input[i].arvTime;
                out[out.length-1].flightHash = input[i].flightHash;
                out[out.length-1].airplaneModel = input[i].airplaneModel;
                out[out.length-1].flightDate = input[i].flightDate;
                out[out.length-1].cost = parseInt($rootScope.adults) * parseInt(out[out.length-1].component1.adultPrice)
                    + parseInt($rootScope.childs) * parseInt(out[out.length-1].component1.childPrice)
                    + parseInt($rootScope.infants) * parseInt(out[out.length-1].component1.infantPrice);
                out[out.length-1].mcost = -1*out[out.length-1].cost;
            }
        }
        return out;
    }
})

app.controller('MainController',function ($rootScope) {
    $rootScope.state = 0;
});

app.controller('SearchController',function ($rootScope, $http) {
    this.searchData = {"src":"THR","dst":"MHD","date":new Date("2017-02-05"),"adults":"1","childs":"1","infants":"1"};

    this.marshal = function () {
        return {
            "src":this.searchData.src,
            "dst":this.searchData.dst,
            "date":this.searchData.date.toISOString().substr(0, 10),
            "adults":this.searchData.adults,
            "childs":this.searchData.childs,
            "infants":this.searchData.infants
        }
    };

    this.SendSearchReq = function () {
        $rootScope.adults = this.searchData.adults;
        $rootScope.childs = this.searchData.childs;
        $rootScope.infants = this.searchData.infants;
        $http.post('services/search',
            this.marshal(this.searchData)).then(
            function(response) {

                $rootScope.flights = response.data;
                $rootScope.state = 1;
            }
        );
    };
});

app.controller('SelectController', function ($rootScope,$http) {
    this.flights = $rootScope.flights;
    this.reserveData = {};
    $rootScope.showWrongCost = false;
    this.airlineSelected = "همه";
    this.classSelected = "همه";
    this.sortType = false;

    this.marshal = function () {
        return {
            "flightHash":this.reserveData.flightHash,
            "className":this.reserveData.className,
            "cost":this.reserveData.cost,
            "adults":$rootScope.adults,
            "childs":$rootScope.childs,
            "infants":$rootScope.infants
        }
    };
    /**
     * @return {string}
     */
    this.FindFlightCount = function () {
        let count = 0;

        for (let i = 0; i < this.flights.length; i++){
            for (let j = 0; j < this.flights[i].seatClassStatuses.length; j++){
                if (this.flights[i].seatClassStatuses[j].component2 == "C") {
                    continue;
                }
                count ++;
            }
        }
        return this.ConvertToPersian(count + "");
    };

    this.ConvertToPersian = function (text) {
        return (text).replace(/0/g, "۰").replace(/1/g, "۱").replace(/2/g, "۲").replace(/3/g, "۳").replace(/4/g, "۴")
            .replace(/5/g, "۵").replace(/6/g, "۶").replace(/7/g, "۷").replace(/8/g, "۸").replace(/9/g, "۹").replace("A", "بیش از ۸");
    };

    /**
     * @return {number}
     */
    this.FindSeatCost = function (seat) {
        return parseInt($rootScope.adults) * parseInt(seat.component1.adultPrice) + parseInt($rootScope.childs) * parseInt(seat.component1.childPrice)
            + parseInt($rootScope.infants) * parseInt(seat.component1.infantPrice);
    };

    this.SendReserveReq = function (seat) {
        this.reserveData.flightHash = seat.flightHash;
        this.reserveData.className = seat.component1.className;
        this.reserveData.cost = seat.cost;

        $http.post('services/reserve',
            this.marshal()).then(
            function(response) {
                $rootScope.ReserveBean = response.data;
                if(response.data.costOkay == true) {
                    $rootScope.state = 2;
                } else {
                    for (var i = 0; i < $rootScope.flights.length; i++){
                        if ($rootScope.flights[i].flightHash == $rootScope.ReserveBean.flight.flightHash){
                            for (var j = 0; j < $rootScope.flights[i].seatClassStatuses.length; j++){
                                if ($rootScope.flights[i].seatClassStatuses[j].component1.id == $rootScope.ReserveBean.seatClass.id) {
                                    $rootScope.flights[i].seatClassStatuses[j].component1.adultPrice = $rootScope.ReserveBean.seatClass.adultPrice;
                                    $rootScope.flights[i].seatClassStatuses[j].component1.childPrice = $rootScope.ReserveBean.seatClass.childPrice;
                                    $rootScope.flights[i].seatClassStatuses[j].component1.infantPrice= $rootScope.ReserveBean.seatClass.infantPrice;
                                }
                            }
                        }
                    }
                    $rootScope.showWrongCost = true;
                    alert("Wrong Price");
                }
            }
        );
    };

    /**
     * @return {boolean}
     */
    this.CheckAirline = function (airline, airlineS) {
        return (airlineS == "همه" || airlineS == airline);
    };

    /**
     * @return {boolean}
     */
    this.CheckClass = function (Class, ClassS) {
        return (ClassS == "همه" || ClassS == Class);
    };

});

app.controller('ReserveController', function ($rootScope, $http) {
    this.ReserveBean = {"flightCode":"IR 452","adultFee":3000,"totalPrice":7000,"numOfPassengers":3,"adults":1,"childs":1,"infants":1,"seatClass":{"adultPrice":3000,"childPrice":2000,"infantPrice":2000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"Y"},"planeModel":"M80","flight":{"flightHash":"1470274236","flightNum":"452","seatClassStatuses":[{"component1":{"adultPrice":3000,"childPrice":2000,"infantPrice":2000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"Y"},"component2":"8"},{"component1":{"adultPrice":4000,"childPrice":3000,"infantPrice":3000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"M"},"component2":"A"},{"component1":{"adultPrice":5000,"childPrice":4000,"infantPrice":4000,"airlineCode":"IR","origCode":"THR","destCode":"MHD","className":"B"},"component2":"C"}],"airlineCode":"IR","origCode":"THR","depTime":{"min":"40","hour":"17"},"destCode":"MHD","arvTime":{"min":"50","hour":"18"},"airplaneModel":"M80","flightDate":{"month":"Feb","day":"05"}},"origCode":"THR","destCode":"MHD","flightDate":"05Feb","adultTotalFee":3000,"infantFee":2000,"childsFee":2000,"maxCap":"8","infantTotalFee":2000,"childTotalFee":2000,"className":"Y"};
    this.ReserveBean = $rootScope.ReserveBean;

    this.passengers = [{}, {}, {}, {}, {}, {}, {}, {}];
    this.ConvertToPersian = function (text) {
        return (text).replace(/0/g, "۰").replace(/1/g, "۱").replace(/2/g, "۲").replace(/3/g, "۳").replace(/4/g, "۴")
            .replace(/5/g, "۵").replace(/6/g, "۶").replace(/7/g, "۷").replace(/8/g, "۸").replace(/9/g, "۹").replace("A", "بیش از ۸");
    };

    this.DecPassengerCount = function () {
        this.ReserveBean.numOfPassengers = this.ReserveBean.numOfPassengers - 1;
        if (this.passengerSelectedDel == "adult"){
            this.ReserveBean.adults --;
        }else if (this.passengerSelectedDel == "child") {
            this.ReserveBean.childs --;
        }else if (this.passengerSelectedDel == "infant"){
            this.ReserveBean.infants --;
        }
    };

    this.IncPassengerCount = function () {
        this.ReserveBean.numOfPassengers = this.ReserveBean.numOfPassengers + 1;
        if (this.passengerSelectedAdd == "adult"){
            this.ReserveBean.adults ++;
        }else if (this.passengerSelectedAdd == "child") {
            this.ReserveBean.childs ++;
        }else if (this.passengerSelectedAdd == "infant"){
            this.ReserveBean.infants ++;
        }
    };


    this.marshal = function () {
        return {
            "adults": this.ReserveBean.adults,
            "childs": this.ReserveBean.childs,
            "infants": this.ReserveBean.infants,
            "flightHash": this.ReserveBean.flight.flightHash,
            "className": this.ReserveBean.seatClass.className,
            "passengers": this.passengers.slice(0, this.ReserveBean.numOfPassengers)
        };
    };

    this.SendReserve = function () {
        $http.post('services/finalize',
            this.marshal(this.searchData)).then(
            function(response) {
                $rootScope.TicketsData = response.data;
                $rootScope.state = 3;
            }
        );
    }

});


app.controller('TicketsController',function ($rootScope) {
   this.TicketsData = $rootScope.TicketsData;
});