
#RUN apt-get -y update
#ADD ./hsqldb /hsqldb
#ADD ./init.sql /
#ADD ./sqltool.rc /root/sqltool.rc
#EXPOSE 9001
#RUN bash -c 'java -classpath hsqldb/lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/IEdb --dbname.0 IEfirstdb'
#RUN java -jar hsqldb/lib/sqltool.jar mem --rcFile=root/sqltool.rc mem init.sql

#ENV TOMCAT_MAJOR_VERSION 8
#ENV TOMCAT_MINOR_VERSION 8.5.11
#ENV CATALINA_HOME /tomcat

#RUN wget -q https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz && \
#	wget -qO- https://archive.apache.org/dist/tomcat/tomcat-${TOMCAT_MAJOR_VERSION}/v${TOMCAT_MINOR_VERSION}/bin/apache-tomcat-${TOMCAT_MINOR_VERSION}.tar.gz.md5 | md5sum -c - && \
#	tar zxf apache-tomcat-*.tar.gz && \
 #	rm apache-tomcat-*.tar.gz && \
 #	mv apache-tomcat* tomcat

#FROM tomcat:8.5.11
#RUN ip addr show
#ADD target/*.war /usr/local/tomcat/webapps/
#RUN bin/shutdown.sh
#RUN bin/startup.sh

FROM tomcat:8.5.11
#RUN apt-get -y update && apt-get install -y default-jdk
ADD ./hsqldb /
#RUN java -jar hsqldb/lib/sqltool.jar mem init.sql
RUN apt-get -y update
#RUN apt-get -y install supervisor
ADD ./target/ticketi.war /usr/local/tomcat/webapps/
COPY ./sqltool.rc ./
COPY ./hsqldb ./hsqldb
#COPY ./run.sh ./
#RUN ls
##RUN echo "[supervisord]" > /etc/supervisord.conf && \
#    echo "nodaemon=true" >> /etc/supervisord.conf && \
#    echo "" >> /etc/supervisord.conf && \
#    echo "[program:dbserver]" >> /etc/supervisord.conf && \
#    echo "command=java -classpath hsqldb/lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/IEdb --dbname.0 IEfirstdb" >> /etc/supervisord.conf && \
#    echo "" >> /etc/supervisord.conf && \
#    echo "[program:mysqld]" >> /etc/supervisord.conf && \
##    echo "command=/usr/bin/mysqld_safe" >> /etc/supervisord.conf && \
#    echo "" >> /etc/supervisord.conf && \
#    echo "[program:apache]" >> /etc/supervisord.conf && \
#    echo "command=catalina.sh -D FOREGROUND" >> /etc/supervisord.conf
##CMD ["nohup", "bash", "-c" , "java -jar hsqldb/lib/sqltool.jar --rcFile=./sqltool.rc mem init.sql", "> startuplog.txt"]
#CMD ["nohup", "bash", "-c" , "java -classpath hsqldb/lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/IEdb --dbname.0 IEfirstdb", "> dbserverlog.txt"]
#CMD ["-d bash -c java -classpath hsqldb/lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/IEdb --dbname.0 IEfirstdb", "-d bash -c java -jar hsqldb/lib/sqltool.jar --rcFile=./sqltool.rc mem init.sql"]
# ENTRYPOINT ["./run.sh"]
#CMD ["catalina.sh", "run"]
#RUN jar -cvf $APP_ROOT/target/ticketi.war *
#RUN cp $APP_ROOT/target/ticketi.war $CATALINA_BASE/webapps
#RUN ls $CATALINA_BASE/webapps
#CMD ["/usr/bin/supervisord"]
