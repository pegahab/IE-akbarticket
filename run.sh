#!/usr/bin/env bash

#nohup java -classpath hsqldb/lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/IEdb --dbname.0 IEfirstdb >test.txt
#nohup java -jar hsqldb/lib/sqltool.jar --rcFile=./sqltool.rc mem init.sql >test.txt

docker build -f Dockerfile -t akbarticket .
docker run --rm -p 9100:8080 akbarticket
docker ps -l
docker exec -d 21727c925acd bash -c "java -classpath hsqldb/lib/hsqldb.jar org.hsqldb.server.Server --database.0 file:hsqldb/IEdb --dbname.0 IEfirstdb"
docker exec -d 21727c925acd bash -c "java -jar hsqldb/lib/sqltool.jar --rcFile=./sqltool.rc mem init.sql"
docker login registry.gitlab.com
docker commit 21727c925acd registry.gitlab.com/pegahab/ie-akbarticket/ticketi
docker push registry.gitlab.com/pegahab/ie-akbarticket/ticketi:latest